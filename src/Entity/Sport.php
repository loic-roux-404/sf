<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SportRepository")
 */
class Sport
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matchmaking", mappedBy="sport")
     */
    private $matchmakings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Court", mappedBy="sport")
     */
    private $courts;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="sport")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TensionHasRacquetStringer", mappedBy="sport")
     */
    private $tensionHasRacquetStringers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RopeReference", mappedBy="sport")
     */
    private $ropeReferences;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RacquetStringer", mappedBy="sport")
     */
    private $racquetStringers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="sports")
     * @ORM\JoinColumn(nullable=true)
     */
    private $users;

    public function __construct()
    {
        $this->matchmakings = new ArrayCollection();
        $this->courts = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->racquetStringerHasSports = new ArrayCollection();
        $this->tensionHasRacquetStringers = new ArrayCollection();
        $this->ropeReferences = new ArrayCollection();
        $this->racquetStringers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|Matchmaking[]
     */
    public function getMatchmakings(): Collection
    {
        return $this->matchmakings;
    }

    public function addMatchmaking(Matchmaking $matchmaking): self
    {
        if (!$this->matchmakings->contains($matchmaking)) {
            $this->matchmakings[] = $matchmaking;
            $matchmaking->setSportId($this);
        }

        return $this;
    }

    public function removeMatchmaking(Matchmaking $matchmaking): self
    {
        if ($this->matchmakings->contains($matchmaking)) {
            $this->matchmakings->removeElement($matchmaking);
            // set the owning side to null (unless already changed)
            if ($matchmaking->getSportId() === $this) {
                $matchmaking->setSportId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Court[]
     */
    public function getCourts(): Collection
    {
        return $this->courts;
    }

    public function addCourt(Court $court): self
    {
        if (!$this->courts->contains($court)) {
            $this->courts[] = $court;
            $court->setSport($this);
        }

        return $this;
    }

    public function removeCourt(Court $court): self
    {
        if ($this->courts->contains($court)) {
            $this->courts->removeElement($court);
            // set the owning side to null (unless already changed)
            if ($court->getSport() === $this) {
                $court->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setSport($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getSport() === $this) {
                $event->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RopeReference[]
     */
    public function getRopeReferences(): Collection
    {
        return $this->ropeReferences;
    }

    public function addRopeReference(RopeReference $ropeReference): self
    {
        if (!$this->ropeReferences->contains($ropeReference)) {
            $this->ropeReferences[] = $ropeReference;
            $ropeReference->setSport($this);
        }

        return $this;
    }

    public function removeRopeReference(RopeReference $ropeReference): self
    {
        if ($this->ropeReferences->contains($ropeReference)) {
            $this->ropeReferences->removeElement($ropeReference);
            // set the owning side to null (unless already changed)
            if ($ropeReference->getSport() === $this) {
                $ropeReference->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RacquetStringer[]
     */
    public function getRacquetStringers(): Collection
    {
        return $this->racquetStringers;
    }

    public function addRacquetStringer(RacquetStringer $racquetStringer): self
    {
        if (!$this->racquetStringers->contains($racquetStringer)) {
            $this->racquetStringers[] = $racquetStringer;
            $racquetStringer->setSport($this);
        }

        return $this;
    }

    public function removeRacquetStringer(RacquetStringer $racquetStringer): self
    {
        if ($this->racquetStringers->contains($racquetStringer)) {
            $this->racquetStringers->removeElement($racquetStringer);
            // set the owning side to null (unless already changed)
            if ($racquetStringer->getSport() === $this) {
                $racquetStringer->setSport(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TensionHasRacquetStringer[]
     */
    public function getTensionHasRacquetStringers(): Collection
    {
        return $this->tensionHasRacquetStringers;
    }

    public function addTensionHasRacquetStringer(TensionHasRacquetStringer $tensionHasRacquetStringer): self
    {
        if (!$this->tensionHasRacquetStringers->contains($tensionHasRacquetStringer)) {
            $this->tensionHasRacquetStringers[] = $tensionHasRacquetStringer;
            $tensionHasRacquetStringer->setSport($this);
        }

        return $this;
    }

    public function removeTensionHasRacquetStringer(TensionHasRacquetStringer $tensionHasRacquetStringer): self
    {
        if ($this->tensionHasRacquetStringers->contains($tensionHasRacquetStringer)) {
            $this->tensionHasRacquetStringers->removeElement($tensionHasRacquetStringer);
            // set the owning side to null (unless already changed)
            if ($tensionHasRacquetStringer->getSport() === $this) {
                $tensionHasRacquetStringer->setSport(null);
            }
        }

        return $this;
    }

    public function getUsers(): ?User
    {
        return $this->users;
    }

    public function setUsers(?User $users): self
    {
        $this->users = $users;

        return $this;
    }
}
