<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RopeReferenceRepository")
 */
class RopeReference
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sport", inversedBy="ropeReferences")
     */
    private $sport;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RacquetStringerHasRopeReference", mappedBy="ropeReferences")
     */
    private $racquetStringerHasRopeReference;

    public function __construct()
    {
        $this->racquetStringerHasRopeReference = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getlabel(): ?string
    {
        return $this->label;
    }

    public function setlabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function __toString()
    {
        return (string) $this->label;
    }

    /**
     * @return Collection|RacquetStringerHasRopeReference[]
     */
    public function getRacquetStringerHasRopeReference(): Collection
    {
        return $this->racquetStringerHasRopeReference;
    }

    public function addRacquetStringerHasRopeReference(RacquetStringerHasRopeReference $racquetStringerHasRopeReference): self
    {
        if (!$this->racquetStringerHasRopeReference->contains($racquetStringerHasRopeReference)) {
            $this->racquetStringerHasRopeReference[] = $racquetStringerHasRopeReference;
            $racquetStringerHasRopeReference->setRopeReferences($this);
        }

        return $this;
    }

    public function removeRacquetStringerHasRopeReference(RacquetStringerHasRopeReference $racquetStringerHasRopeReference): self
    {
        if ($this->racquetStringerHasRopeReference->contains($racquetStringerHasRopeReference)) {
            $this->racquetStringerHasRopeReference->removeElement($racquetStringerHasRopeReference);
            // set the owning side to null (unless already changed)
            if ($racquetStringerHasRopeReference->getRopeReferences() === $this) {
                $racquetStringerHasRopeReference->setRopeReferences(null);
            }
        }

        return $this;
    }
}
