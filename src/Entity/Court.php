<?php

namespace App\Entity;

use App\Entity\Utils\Image;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CourtRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable()
 */
class Court extends Prestation
{
    /* @var string name of the route */
    const CREATE_ROUTE_NAME = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sport", inversedBy="courts")
     */
    private $sport;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address", inversedBy="courts")
     */
    private $address;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbCourt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isIndoor;

    /**
     * @ORM\Column(type="float")
     */
    private $fullPrice;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $lowPrice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $urlLink;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeSlotCourt", mappedBy="court", orphanRemoval=true)
     */
    private $timeSlots;

    /**
     * @Vich\UploadableField(mapping="court", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    use Image;

    public function __construct()
    {
        $this->matchmakings = new ArrayCollection();
        $this->timeSlots = new ArrayCollection();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getNbCourt(): ?int
    {
        return $this->nbCourt;
    }

    public function setNbCourt(int $nbCourt): self
    {
        $this->nbCourt = $nbCourt;

        return $this;
    }

    public function getIsIndoor(): ?bool
    {
        return $this->isIndoor;
    }

    public function setIsIndoor(bool $isIndoor): self
    {
        $this->isIndoor = $isIndoor;

        return $this;
    }

    public function getFullPrice(): ?float
    {
        return $this->fullPrice;
    }

    public function setFullPrice(float $fullPrice): self
    {
        $this->fullPrice = $fullPrice;

        return $this;
    }

    public function getLowPrice(): ?float
    {
        return $this->lowPrice;
    }

    public function setLowPrice(?float $lowPrice): self
    {
        $this->lowPrice = $lowPrice;

        return $this;
    }

    public function getUrlLink(): ?string
    {
        return $this->urlLink;
    }

    public function setUrlLink(?string $urlLink): self
    {
        $this->urlLink = $urlLink;

        return $this;
    }

    /**
     * @return Collection|TimeSlotCourt[]
     */
    public function getTimeSlots(): Collection
    {
        return $this->timeSlots;
    }

    public function addTimeSlot(TimeSlotCourt $timeSlot): self
    {
        if (!$this->timeSlots->contains($timeSlot)) {
            $this->timeSlots[] = $timeSlot;
            $timeSlot->setCourt($this);
        }

        return $this;
    }

    public function removeTimeSlot(TimeSlotCourt $timeSlot): self
    {
        if ($this->timeSlots->contains($timeSlot)) {
            $this->timeSlots->removeElement($timeSlot);
            // set the owning side to null (unless already changed)
            if ($timeSlot->getCourt() === $this) {
                $timeSlot->setCourt(null);
            }
        }

        return $this;
    }
}
