<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AddressRepository")
 */
class Address
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $postalCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $country;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\OneToMany(targetEntity="Event", mappedBy="address")
     */
    private $events;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Court", mappedBy="address")
     */
    private $courts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matchmaking", mappedBy="externalAddress")
     */
    private $matchmakings;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->matchmakings = new ArrayCollection();
        $this->courts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setAddress($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getAddress() === $this) {
                $event->setAddress(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Matchmaking[]
     */
    public function getMatchmakings(): Collection
    {
        return $this->matchmakings;
    }

    public function addMatchmaking(Matchmaking $matchmaking): self
    {
        if (!$this->matchmakings->contains($matchmaking)) {
            $this->matchmakings[] = $matchmaking;
            $matchmaking->setExternalAddress($this);
        }

        return $this;
    }

    public function removeMatchmaking(Matchmaking $matchmaking): self
    {
        if ($this->matchmakings->contains($matchmaking)) {
            $this->matchmakings->removeElement($matchmaking);
            // set the owning side to null (unless already changed)
            if ($matchmaking->getExternalAddress() === $this) {
                $matchmaking->setExternalAddress(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Court[]
     */
    public function getCourts(): Collection
    {
        return $this->courts;
    }

    public function addCourt(Court $court): self
    {
        if (!$this->courts->contains($court)) {
            $this->courts[] = $court;
            $court->setSport($this);
        }

        return $this;
    }

    public function removeCourt(Court $court): self
    {
        if ($this->courts->contains($court)) {
            $this->courts->removeElement($court);
            // set the owning side to null (unless already changed)
            if ($court->getSport() === $this) {
                $court->setSport(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->street;
    }
}
