<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TensionHasRacquetStringerRepository")
 */
class TensionHasRacquetStringer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $minTension;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $maxTension;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RacquetStringer", inversedBy="tensionHasRacquetStringers")
     */
    private $racquetStringer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sport", inversedBy="tensionHasRacquetStringers")
     */
    private $sport;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMinTension(): ?string
    {
        return $this->minTension;
    }

    public function setMinTension(string $minTension): self
    {
        $this->minTension = $minTension;

        return $this;
    }

    public function getMaxTension(): ?string
    {
        return $this->maxTension;
    }

    public function setMaxTension(string $maxTension): self
    {
        $this->maxTension = $maxTension;

        return $this;
    }

    public function getRacquetStringer(): ?RacquetStringer
    {
        return $this->racquetStringer;
    }

    public function setRacquetStringer(?RacquetStringer $racquetStringer): self
    {
        $this->racquetStringer = $racquetStringer;

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }
}
