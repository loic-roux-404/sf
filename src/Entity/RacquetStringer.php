<?php

namespace App\Entity;

use App\Entity\Utils\Image;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RacquetStringerRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class RacquetStringer extends Prestation
{
    /* @var string Name of the route */
    const CREATE_ROUTE_NAME = null;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $restitutionPeriod;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paymentCash;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paymentCard;

    /**
     * @ORM\Column(type="boolean")
     */
    private $paymentCheck;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\RacquetStringerHasRopeReference", mappedBy="racquetStringer", orphanRemoval=true)
     */
    private $ropeReferences;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeSlotStringer", mappedBy="racquetStringer", orphanRemoval=true)
     */
    private $timeSlots;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sport", inversedBy="racquetStringers")
     */
    private $sport;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TensionHasRacquetStringer", mappedBy="racquetStringer", orphanRemoval=true)
     */
    private $tensionHasRacquetStringers;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $maxTension;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minTension;

    /**
     * @Vich\UploadableField(mapping="stringer", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    use Image;

    public function __construct()
    {
        $this->ropeReferences = new ArrayCollection();
        $this->timeSlots = new ArrayCollection();
        $this->tensionHasRacquetStringers = new ArrayCollection();
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRestitutionPeriod(): ?float
    {
        return $this->restitutionPeriod;
    }

    public function setRestitutionPeriod(float $restitutionPeriod): self
    {
        $this->restitutionPeriod = $restitutionPeriod;

        return $this;
    }

    public function getPaymentCash(): ?bool
    {
        return $this->paymentCash;
    }

    public function setPaymentCash(bool $paymentCash): self
    {
        $this->paymentCash = $paymentCash;

        return $this;
    }

    public function getPaymentCard(): ?bool
    {
        return $this->paymentCard;
    }

    public function setPaymentCard(bool $paymentCard): self
    {
        $this->paymentCard = $paymentCard;

        return $this;
    }

    public function getPaymentCheck(): ?bool
    {
        return $this->paymentCheck;
    }

    public function setPaymentCheck(bool $paymentCheck): self
    {
        $this->paymentCheck = $paymentCheck;

        return $this;
    }

    /**
     * @return Collection|RacquetStringerHasRopeReference[]
     */
    public function getRopeReferences(): Collection
    {
        return $this->ropeReferences;
    }

    public function addRopeReference(RacquetStringerHasRopeReference $ropeReference): self
    {
        if (!$this->ropeReferences->contains($ropeReference)) {
            $this->ropeReferences[] = $ropeReference;
            $ropeReference->setRacquetStringer($this);
        }

        return $this;
    }

    public function removeRopeReference(RacquetStringerHasRopeReference $ropeReference): self
    {
        if ($this->ropeReferences->contains($ropeReference)) {
            $this->ropeReferences->removeElement($ropeReference);
            // set the owning side to null (unless already changed)
            if ($ropeReference->getRacquetStringer() === $this) {
                $ropeReference->setRacquetStringer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TimeSlotStringer[]
     */
    public function getTimeSlots(): Collection
    {
        return $this->timeSlots;
    }

    public function addTimeSlot(TimeSlotStringer $timeSlot): self
    {
        if (!$this->timeSlots->contains($timeSlot)) {
            $this->timeSlots[] = $timeSlot;
            $timeSlot->setRacquetStringer($this);
        }

        return $this;
    }

    public function removeTimeSlot(TimeSlotStringer $timeSlot): self
    {
        if ($this->timeSlots->contains($timeSlot)) {
            $this->timeSlots->removeElement($timeSlot);
            // set the owning side to null (unless already changed)
            if ($timeSlot->getRacquetStringer() === $this) {
                $timeSlot->setRacquetStringer(null);
            }
        }

        return $this;
    }

    public function getSport(): ?Sport
    {
        return $this->sport;
    }

    public function setSport(?Sport $sport): self
    {
        $this->sport = $sport;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getMaxTension(): ?int
    {
        return $this->maxTension;
    }

    public function setMaxTension(int $maxTension): self
    {
        $this->maxTension = $maxTension;

        return $this;
    }

    public function getMinTension(): ?int
    {
        return $this->minTension;
    }

    public function setMinTension(int $minTension): self
    {
        $this->minTension = $minTension;

        return $this;
    }

    /**
     * @return Collection|TensionHasRacquetStringer[]
     */
    public function getTensionHasRacquetStringers(): Collection
    {
        return $this->tensionHasRacquetStringers;
    }

    public function addTensionHasRacquetStringer(TensionHasRacquetStringer $tensionHasRacquetStringer): self
    {
        if (!$this->tensionHasRacquetStringers->contains($tensionHasRacquetStringer)) {
            $this->tensionHasRacquetStringers[] = $tensionHasRacquetStringer;
            $tensionHasRacquetStringer->setRacquetStringer($this);
        }

        return $this;
    }

    public function removeTensionHasRacquetStringer(TensionHasRacquetStringer $tensionHasRacquetStringer): self
    {
        if ($this->tensionHasRacquetStringers->contains($tensionHasRacquetStringer)) {
            $this->tensionHasRacquetStringers->removeElement($tensionHasRacquetStringer);
            // set the owning side to null (unless already changed)
            if ($tensionHasRacquetStringer->getRacquetStringer() === $this) {
                $tensionHasRacquetStringer->setRacquetStringer(null);
            }
        }

        return $this;
    }
}
