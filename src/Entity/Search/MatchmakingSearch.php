<?php

namespace App\Entity\Search;

use Doctrine\Common\Collections\ArrayCollection;

class MatchmakingSearch
{
    /**
     * @var ArrayCollection|null
     */
    private $sports;

    /**
     * @var ArrayCollection|null
     */
    private $levels;

    /**
     * @var int|null
     */
    private $maxPrice;

    /**
     * @var string|null
     */
    private $localisation;

    /**
     * @var string|null
     */
    private $latitude;

    /**
     * @var string|null
     */
    private $longitude;

    /**
     * @var int|null
     */
    private $perimeter;

    /**
     * @var int|null
     */
    private $duration;

    /**
     * @var string|null
     */
    private $datetime;

    public function getSports(): ?ArrayCollection
    {
        return $this->sports;
    }

    public function setSports(?ArrayCollection $sports): MatchmakingSearch
    {
        $this->sports = $sports;

        return $this;
    }

    public function getLevels(): ?ArrayCollection
    {
        return $this->levels;
    }

    public function setLevels(?ArrayCollection $levels): MatchmakingSearch
    {
        $this->levels = $levels;

        return $this;
    }

    public function getMaxPrice(): ?int
    {
        return $this->maxPrice;
    }

    public function setMaxPrice(?int $maxPrice): MatchmakingSearch
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    public function getLocalisation(): ?string
    {
        return $this->localisation;
    }

    public function setLocalisation(?string $localisation): MatchmakingSearch
    {
        $this->localisation = $localisation;

        return $this;
    }

    public function getPerimeter(): ?int
    {
        return $this->perimeter;
    }

    public function setPerimeter(?int $perimeter): MatchmakingSearch
    {
        $this->perimeter = $perimeter;

        return $this;
    }

    public function getDuration(): ?int
    {
        return $this->duration;
    }

    public function setDuration(?int $duration): MatchmakingSearch
    {
        $this->duration = $duration;

        return $this;
    }

    public function getDatetime(): ?string
    {
        return $this->datetime;
    }

    public function setDatetime(?string $datetime): MatchmakingSearch
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function setLatitude(?string $latitude): MatchmakingSearch
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function setLongitude(?string $longitude): MatchmakingSearch
    {
        $this->longitude = $longitude;

        return $this;
    }
}
