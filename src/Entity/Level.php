<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LevelRepository")
 */
class Level
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $slug;

    /**
     * @ORM\Column(type="integer")
     */
    private $sport_level_user_sport_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $sport_level_user_user_id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Matchmaking", mappedBy="level")
     */
    private $matchmakings;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Event", mappedBy="level")
     */
    private $events;

    public function __construct()
    {
        $this->matchmakings = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getSportLevelUserSportId(): ?int
    {
        return $this->sport_level_user_sport_id;
    }

    public function setSportLevelUserSportId(int $sport_level_user_sport_id): self
    {
        $this->sport_level_user_sport_id = $sport_level_user_sport_id;

        return $this;
    }

    public function getSportLevelUserUserId(): ?int
    {
        return $this->sport_level_user_user_id;
    }

    public function setSportLevelUserUserId(int $sport_level_user_user_id): self
    {
        $this->sport_level_user_user_id = $sport_level_user_user_id;

        return $this;
    }

    /**
     * @return Collection|Matchmaking[]
     */
    public function getMatchmakings(): Collection
    {
        return $this->matchmakings;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->getLabel();
    }
}
