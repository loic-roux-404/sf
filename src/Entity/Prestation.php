<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({
 *     "event" = "Event",
 *     "shop" = "Shop",
 *     "racquet_stringer" = "RacquetStringer",
 *     "court" = "Court"
 * })
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable()
 */
abstract class Prestation implements Utils\PrestationInterface
{
    use Utils\Id;
    use Utils\Name;
    use Utils\UpdatedAt;
    use Utils\PartnerUser;

    /*
     * Fix for VichUploader
     */
    public function __toString()
    {
        return (string) $this->name;
    }
}
