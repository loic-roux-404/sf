<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HighlightPartnerRepository")
 */
class HighlightPartner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $partner;

    /**
     * @ORM\Column(type="integer")
     */
    private $displayOrder;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\HighlightPartnerType")
     * @ORM\JoinColumn(nullable=false)
     */
    private $highlightType;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPartner(): ?User
    {
        return $this->partner;
    }

    public function setPartner(?User $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function getDisplayOrder(): ?int
    {
        return $this->displayOrder;
    }

    public function setDisplayOrder(int $displayOrder): self
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    public function getHighlightType(): ?HighlightPartnerType
    {
        return $this->highlightType;
    }

    public function setHighlightType(?HighlightPartnerType $highlightType): self
    {
        $this->highlightType = $highlightType;

        return $this;
    }
}
