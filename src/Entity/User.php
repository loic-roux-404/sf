<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sport", mappedBy="users")
     * @JoinTable(name="user_sport",
     *      joinColumns={@JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="sport_id", referencedColumnName="id")}
     * )
     */
    private $sports;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Address")
     */
    private $address;

    /**
     * @ORM\Column(type="boolean")
     */
    private $partner = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $partnerValidated = false;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $partnerValidationDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $socialReason;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $commercialName;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $newsletter = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private $notification = false;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Prestation", mappedBy="user", cascade={"persist", "remove"})
     */
    private $partnerPresta;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeSlotUser", mappedBy="user", orphanRemoval=true)
     */
    private $timeSlots;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="user", orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserMatchmaking", mappedBy="user")
     */
    private $matchmakings;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $partnerSiren;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $partnerReason;

    public function __construct()
    {
        parent::__construct();
        $this->sports = new ArrayCollection();
        $this->timeSlots = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->matchmakings = new ArrayCollection();
        $this->partnerPresta = new ArrayCollection();
    }

    /**
     * @return Collection|Sport[]
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        if ($this->sports->contains($sport)) {
            $this->sports->removeElement($sport);
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getPartner(): ?bool
    {
        return $this->partner;
    }

    public function setPartner(bool $partner): self
    {
        $this->partner = $partner;

        return $this;
    }

    public function getPartnerValidated(): ?bool
    {
        return $this->partnerValidated;
    }

    public function setPartnerValidated(?bool $partnerValidated): self
    {
        $this->partnerValidated = $partnerValidated;

        return $this;
    }

    public function getPartnerValidationDate(): ?\DateTimeInterface
    {
        return $this->partnerValidationDate;
    }

    public function setPartnerValidationDate(?\DateTimeInterface $partnerValidationDate): self
    {
        $this->partnerValidationDate = $partnerValidationDate;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getSocialReason(): ?string
    {
        return $this->socialReason;
    }

    public function setSocialReason(?string $socialReason): self
    {
        $this->socialReason = $socialReason;

        return $this;
    }

    public function getCommercialName(): ?string
    {
        return $this->commercialName;
    }

    public function setCommercialName(?string $commercialName): self
    {
        $this->commercialName = $commercialName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param mixed $newsletter
     */
    public function setNewsletter($newsletter): void
    {
        $this->newsletter = $newsletter;
    }

    public function getNotification(): ?bool
    {
        return $this->notification;
    }

    public function setNotification(bool $notification): self
    {
        $this->notification = $notification;

        return $this;
    }

    /**
     * @return Collection|TimeSlotUser[]
     */
    public function getTimeSlots(): Collection
    {
        return $this->timeSlots;
    }

    public function addTimeSlot(TimeSlotUser $timeSlot): self
    {
        if (!$this->timeSlots->contains($timeSlot)) {
            $this->timeSlots[] = $timeSlot;
            $timeSlot->setUser($this);
        }

        return $this;
    }

    public function removeTimeSlot(TimeSlotUser $timeSlot): self
    {
        if ($this->timeSlots->contains($timeSlot)) {
            $this->timeSlots->removeElement($timeSlot);
            // set the owning side to null (unless already changed)
            if ($timeSlot->getUser() === $this) {
                $timeSlot->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUser($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getUser() === $this) {
                $message->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserMatchmaking[]
     */
    public function getMatchmakings(): Collection
    {
        return $this->matchmakings;
    }

    public function addMatchmaking(UserMatchmaking $matchmaking): self
    {
        if (!$this->matchmakings->contains($matchmaking)) {
            $this->matchmakings[] = $matchmaking;
            $matchmaking->setUser($this);
        }

        return $this;
    }

    public function removeMatchmaking(UserMatchmaking $matchmaking): self
    {
        if ($this->matchmakings->contains($matchmaking)) {
            $this->matchmakings->removeElement($matchmaking);
            // set the owning side to null (unless already changed)
            if ($matchmaking->getUser() === $this) {
                $matchmaking->setUser(null);
            }
        }

        return $this;
    }

    public function getPartnerSiren(): ?string
    {
        return $this->partnerSiren;
    }

    public function setPartnerSiren(?string $partnerSiren): self
    {
        $this->partnerSiren = $partnerSiren;

        return $this;
    }

    public function getPartnerReason(): ?string
    {
        return $this->partnerReason;
    }

    public function setPartnerReason(?string $partnerReason): self
    {
        $this->partnerReason = $partnerReason;

        return $this;
    }

    public function __toString()
    {
        return __CLASS__;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Prestation[]
     */
    public function getPartnerPresta(): Collection
    {
        return $this->partnerPresta;
    }

    public function addPartnerPresta(Prestation $partnerPrestum): self
    {
        if (!$this->partnerPresta->contains($partnerPrestum)) {
            $this->partnerPresta[] = $partnerPrestum;
            $partnerPrestum->setUser($this);
        }

        return $this;
    }

    public function removePartnerPresta(Prestation $partnerPrestum): self
    {
        if ($this->partnerPresta->contains($partnerPrestum)) {
            $this->partnerPresta->removeElement($partnerPrestum);
            // set the owning side to null (unless already changed)
            if ($partnerPrestum->getUser() === $this) {
                $partnerPrestum->setUser(null);
            }
        }

        return $this;
    }
}
