<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserMatchmakingRepository")
 */
class UserMatchmaking
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="matchmakings")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matchmaking", inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $matchmaking;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isOrganizer;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $cancel;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getMatchmaking(): ?Matchmaking
    {
        return $this->matchmaking;
    }

    public function setMatchmaking(?Matchmaking $matchmaking): self
    {
        $this->matchmaking = $matchmaking;

        return $this;
    }

    public function getIsOrganizer(): ?bool
    {
        return $this->isOrganizer;
    }

    public function setIsOrganizer(?bool $isOrganizer): self
    {
        $this->isOrganizer = $isOrganizer;

        return $this;
    }

    public function getCancel(): ?bool
    {
        return $this->cancel;
    }

    public function setCancel(?bool $cancel): self
    {
        $this->cancel = $cancel;

        return $this;
    }
}
