<?php

namespace App\Entity;

use App\Entity\Utils\Image;
use App\Entity\Utils\PartnerUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShopRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class Shop extends Prestation
{
    /* @var string name of the route */
    const CREATE_ROUTE_NAME = 'createShop';

    use PartnerUser;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Brand", inversedBy="brands", cascade={"persist"})
     */
    private $brands;

    /**
     * @ORM\ManyToOne(targetEntity="Address", inversedBy="shops", cascade={"persist", "remove"})
     */
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="Sport")
     */
    private $sports;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TimeSlotShop", mappedBy="shop", orphanRemoval=true)
     */
    private $timeSlots;

    /**
     * @Vich\UploadableField(mapping="shop", fileNameProperty="image")
     *
     * @var File
     */
    private $imageFile;

    use Image;

    public function __construct()
    {
        $this->brands = new ArrayCollection();
        $this->sports = new ArrayCollection();
        $this->timeSlots = new ArrayCollection();
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(?string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return Collection|Brand[]
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }

    /**
     * @var Collection|Brand[]
     */
    public function setBrands($brands): self
    {
        $this->brands->clear();

        if ($brands instanceof Brand) {
            $this->brands = new ArrayCollection([$brands]);
        } elseif (is_array($brands)) {
            $this->brands = new ArrayCollection($brands);
        }

        return $this;
    }

    public function addBrand(Brand $brand): self
    {
        if (!$this->brands->contains($brand)) {
            $this->brands[] = $brand;
        }

        return $this;
    }

    public function removeBrand(Brand $brand): self
    {
        if ($this->brands->contains($brand)) {
            $this->brands->removeElement($brand);
        }

        return $this;
    }

    /**
     * @return Collection|Sport[]
     */
    public function getSports(): Collection
    {
        return $this->sports;
    }

    public function addSport(Sport $sport): self
    {
        if (!$this->sports->contains($sport)) {
            $this->sports[] = $sport;
        }

        return $this;
    }

    public function removeSport(Sport $sport): self
    {
        if ($this->sports->contains($sport)) {
            $this->sports->removeElement($sport);
        }

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|TimeSlotShop[]
     */
    public function getTimeSlots(): Collection
    {
        return $this->timeSlots;
    }

    public function addTimeSlot(TimeSlotShop $timeSlot): self
    {
        if (!$this->timeSlots->contains($timeSlot)) {
            $this->timeSlots[] = $timeSlot;
            $timeSlot->setShop($this);
        }

        return $this;
    }

    public function removeTimeSlot(TimeSlotShop $timeSlot): self
    {
        if ($this->timeSlots->contains($timeSlot)) {
            $this->timeSlots->removeElement($timeSlot);
            // set the owning side to null (unless already changed)
            if ($timeSlot->getShop() === $this) {
                $timeSlot->setShop(null);
            }
        }

        return $this;
    }

    public function getAddress(): ?Address
    {
        return $this->address;
    }

    public function setAddress(?Address $address): self
    {
        $this->address = $address;

        return $this;
    }
}
