<?php

namespace App\Entity\Utils;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;

trait PartnerUser
{
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="partnerPresta", cascade={"persist", "remove"})
     */
    private $user;

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }
}
