<?php

namespace App\Entity\Utils;

use App\Entity\User;

/**
 * Interface Prestation
 * Use this interface with Abstract Prestation to create necessary ORM fields for a p partner prestation
 * Properties to implement in your function.
 */
interface PrestationInterface
{
    /**
     * Implement a const to get the associated entity route.
     * Used to find the correct Prestation edit form on dashboard or
     * any other entity which need to show random Prestations
     * HOW TO USE :
     * Define a cont REATE_ROUTE_NAME on top of a prestation class.
     *
     * @var string CREATE_ROUTE_NAME
     */
    public function getName(): ?string;

    public function setName(string $name);

    public function getUser(): ?User;

    public function setUser(User $user);

    public function getImageFile();

    public function setImage($image);

    public function getUpdatedAt(): \DateTime;

    public function setUpdatedAt(?\DateTime $updatedAt);

    public function setOnPrePersistUpdatedAt();
}
