<?php

namespace App\Entity\Utils;

use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Trait Image.
 *
 * Use the vich property `ImageFile` and the next annotation inside your class before
 * next to this trait instanciation
 * /**
 * * @Vich\UploadableField(mapping='mapping_name' fileNameProperty="image")
 * * private $imageFile;
 * *\/
 *
 * @var File
 */
trait Image
{
    private $imageMapping;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @var string
     */
    private $image;

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
}
