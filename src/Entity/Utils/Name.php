<?php

namespace App\Entity\Utils;

use Doctrine\ORM\Mapping as ORM;

trait Name
{
    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
