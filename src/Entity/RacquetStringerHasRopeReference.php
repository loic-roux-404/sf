<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\RacquetStringerHasRopeReferenceRepository")
 */
class RacquetStringerHasRopeReference
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RacquetStringer", inversedBy="ropeReferences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $racquetStringer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\RopeReference", inversedBy="racquetStringerHasRopeReference")
     * @ORM\JoinColumn(nullable=false)
     */
    private $ropeReferences;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRacquetStringer(): ?RacquetStringer
    {
        return $this->racquetStringer;
    }

    public function setRacquetStringer(?RacquetStringer $racquetStringer): self
    {
        $this->racquetStringer = $racquetStringer;

        return $this;
    }

    public function getRopeReferences(): ?RopeReference
    {
        return $this->ropeReferences;
    }

    public function setRopeReferences(?RopeReference $ropeReferences): self
    {
        $this->ropeReferences = $ropeReferences;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
