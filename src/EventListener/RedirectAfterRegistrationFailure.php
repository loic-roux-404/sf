<?php

namespace App\EventListener;

use App\Entity\User;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;

class RedirectAfterRegistrationFailure extends AbstractController implements EventSubscriberInterface
{
    private $router;

    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    public function onRegistrationFailure(FormEvent $event)
    {
        $this->addFlash('danger', $event->getForm()->getData());

        $userRepository = $this->getDoctrine()->getRepository(User::class);
        if (!empty($userRepository->findOneBy(['username' => $event->getForm()->getData()->getUsername()]))) {
            $this->addFlash('danger', "Ce nom d'utilisateur est déjà utilisé");
        } elseif (!empty($userRepository->findOneBy(['email' => $event->getForm()->getData()->getEmail()]))) {
            $this->addFlash('danger', 'Cette adresse email est déjà utilisée');
        } else {
            $this->addFlash('danger', 'Les mots de passe ne correspondent pas');
        }

        $url = $this->router->generate('home');
        $response = new RedirectResponse($url);
        $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_FAILURE => 'onRegistrationFailure',
        ];
    }
}
