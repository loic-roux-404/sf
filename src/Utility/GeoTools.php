<?php

namespace App\Utility;

class GeoTools
{
    /**
     * @param $coordinateFrom
     * @param $coordinateTo
     * @param int $earthRadius
     *
     * @return float|int
     */
    public static function getDistanceBetweenTwoPoints(array $coordinateFrom, array $coordinateTo, $earthRadius = 6371)
    {
        $latFrom = deg2rad($coordinateFrom[0]);
        $lonFrom = deg2rad($coordinateFrom[1]);
        $latTo = deg2rad($coordinateTo[0]);
        $lonTo = deg2rad($coordinateTo[1]);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return $angle * $earthRadius;
    }

    /**
     * @return array|bool
     */
    public static function getCoordinatesFromFullAddress(string $address)
    {
        $url = 'https://api-adresse.data.gouv.fr/search/?q='.str_replace(' ', '+', $address).'&autocomplete=0&limit=1';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        $response = curl_exec($curl);
        curl_close($curl);

        $addresses = json_decode($response);

        dump($addresses);

        if (!empty($addresses->features)) {
            $coordinates = [
                $addresses->features[0]->geometry->coordinates[1],
                $addresses->features[0]->geometry->coordinates[0],
            ];
        } else {
            $coordinates = false;
        }

        return $coordinates;
    }
}
