<?php

namespace App\Repository;

use App\Entity\Matchmaking;
use App\Entity\Search\MatchmakingSearch;
use App\Utility\GeoTools;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Matchmaking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Matchmaking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Matchmaking[]    findAll()
 * @method Matchmaking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MatchmakingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Matchmaking::class);
    }

    /**
     * @param $currentUser
     * @param $search MatchmakingSearch
     */
    public function findByDateAndUserSports($currentUser, MatchmakingSearch $search): array
    {
        $currentMonthDateTime = date('Y-m-d H:i:s');
        $query = $this
            ->createQueryBuilder('m')
            ->select('m')
            ->andWhere('m.start >= :val')
            ->setParameter('val', $currentMonthDateTime)
            ->orderBy('m.start', 'ASC');

        if (!empty($search->getDuration())) {
            $query = $query->andWhere('(m.end - m.start) <= :duration')->setParameter('duration', $search->getDuration());
        }
        if (!empty($search->getMaxPrice())) {
            $query = $query->andWhere('m.price <= :max_price')->setParameter('max_price', $search->getMaxPrice());
        }
        if (!empty($search->getSports()) && !empty($search->getSports()->toArray())) {
            $query = $query->andWhere('m.sport IN(:sports)')->setParameter('sports', $search->getSports());
        }
        if (!empty($search->getLevels()) && !empty($search->getLevels()->toArray())) {
            $query = $query->andWhere('m.level IN(:levels)')->setParameter('levels', $search->getLevels());
        }
        if (!empty($search->getDatetime())) {
            $date = str_replace(' - ', ' ', $search->getDatetime());
            $date = str_replace('/', '-', $date);
            $date = date('Y-m-d H:i:s', (strtotime($date) - 3600));
            $query = $query->andWhere('m.start >= :datetime')->setParameter('datetime', $date);
        }

        $listMatchmakings = $query->getQuery()->getResult(Query::HYDRATE_OBJECT);

        if (!empty($currentUser) && empty($search->getSports())) {
            $sports = [];
            foreach ($currentUser->getSports() as $key => $sport) {
                $sports[] = $sport->getName();
            }
            foreach ($listMatchmakings as $key => $matchmaking) {
                if (!(in_array($matchmaking['sport_name'], $sports))) {
                    unset($listMatchmakings[$key]);
                }
            }
        }

        if (!empty($search->getLocalisation())) {
            if (!empty($search->getLatitude()) && $search->getLongitude()) {
                $searchCoordinates = [$search->getLatitude(), $search->getLongitude()];
                foreach ($listMatchmakings as $key => $matchmaking) {
                    $matchmakingCoordinates = [$matchmaking->getCourt()->getAddress()->getLatitude(), $matchmaking->getCourt()->getAddress()->getLongitude()];
                    if (GeoTools::getDistanceBetweenTwoPoints($searchCoordinates, $matchmakingCoordinates) > $search->getPerimeter()) {
                        unset($listMatchmakings[$key]);
                    }
                }
            } else {
                $coordinates = GeoTools::getCoordinatesFromFullAddress($search->getLocalisation());
                if (!empty($coordinates)) {
                    foreach ($listMatchmakings as $key => $matchmaking) {
                        $matchmakingCoordinates = [$matchmaking->getCourt()->getAddress()->getLatitude(), $matchmaking->getCourt()->getAddress()->getLongitude()];
                        if (GeoTools::getDistanceBetweenTwoPoints($coordinates, $matchmakingCoordinates) > $search->getPerimeter()) {
                            unset($listMatchmakings[$key]);
                        }
                    }
                }
            }
        }

        return $listMatchmakings;
    }
}
