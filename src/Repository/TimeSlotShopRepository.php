<?php

namespace App\Repository;

use App\Entity\TimeSlotShop;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TimeSlotShop|null find($id, $lockMode = null, $lockVersion = null)
 * @method TimeSlotShop|null findOneBy(array $criteria, array $orderBy = null)
 * @method TimeSlotShop[]    findAll()
 * @method TimeSlotShop[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TimeSlotShopRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TimeSlotShop::class);
    }
}
