<?php

namespace App\Repository;

use App\Entity\RacquetStringer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RacquetStringer|null find($id, $lockMode = null, $lockVersion = null)
 * @method RacquetStringer|null findOneBy(array $criteria, array $orderBy = null)
 * @method RacquetStringer[]    findAll()
 * @method RacquetStringer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RacquetStringerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RacquetStringer::class);
    }

    // /**
    //  * @return RacquetStringer[] Returns an array of RacquetStringer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RacquetStringer
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
