<?php

namespace App\Repository;

use App\Entity\HighlightPartner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HighlightPartner|null find($id, $lockMode = null, $lockVersion = null)
 * @method HighlightPartner|null findOneBy(array $criteria, array $orderBy = null)
 * @method HighlightPartner[]    findAll()
 * @method HighlightPartner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HighlightPartnerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HighlightPartner::class);
    }

    // /**
    //  * @return HighlightPartner[] Returns an array of HighlightPartner objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HighlightPartner
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
