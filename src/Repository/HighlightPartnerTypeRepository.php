<?php

namespace App\Repository;

use App\Entity\HighlightPartnerType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method HighlightPartnerType|null find($id, $lockMode = null, $lockVersion = null)
 * @method HighlightPartnerType|null findOneBy(array $criteria, array $orderBy = null)
 * @method HighlightPartnerType[]    findAll()
 * @method HighlightPartnerType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HighlightPartnerTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HighlightPartnerType::class);
    }

    // /**
    //  * @return HighlightPartnerType[] Returns an array of HighlightPartnerType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HighlightPartnerType
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
