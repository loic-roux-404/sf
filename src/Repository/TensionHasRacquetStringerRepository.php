<?php

namespace App\Repository;

use App\Entity\TensionHasRacquetStringer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TensionHasRacquetStringer|null find($id, $lockMode = null, $lockVersion = null)
 * @method TensionHasRacquetStringer|null findOneBy(array $criteria, array $orderBy = null)
 * @method TensionHasRacquetStringer[]    findAll()
 * @method TensionHasRacquetStringer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TensionHasRacquetStringerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TensionHasRacquetStringer::class);
    }

    // /**
    //  * @return TensionHasRacquetStringer[] Returns an array of TensionHasRacquetStringer objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TensionHasRacquetStringer
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
