<?php

namespace App\Repository;

use App\Entity\RacquetStringerHasRopeReference;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method RacquetStringerHasRopeReference|null find($id, $lockMode = null, $lockVersion = null)
 * @method RacquetStringerHasRopeReference|null findOneBy(array $criteria, array $orderBy = null)
 * @method RacquetStringerHasRopeReference[]    findAll()
 * @method RacquetStringerHasRopeReference[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RacquetStringerHasRopeReferenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RacquetStringerHasRopeReference::class);
    }

    // /**
    //  * @return RacquetStringerHasRopeReference[] Returns an array of RacquetStringerHasRopeReference objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RacquetStringerHasRopeReference
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
