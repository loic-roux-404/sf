<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BrandFixtures extends Fixture
{
    protected $brands;

    public $nbBrands;

    public function __construct()
    {
        $this->brands = [
            'Ashaway',
            'Babolat',
            'Dunlop',
            'Forza',
            'Head',
            'Karakal',
            'Oliver',
            'Prince',
            'Salming',
            'Tecnifibre',
            'Victor',
            'Wilson',
            'Yonex',
        ];

        $this->nbBrands = count($this->brands);
    }

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->brands as $brandName) {
            $brand = new Brand();
            $brand->setLabel($brandName);
            $manager->persist($brand);
        }

        $manager->flush();
    }
}
