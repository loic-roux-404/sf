<?php

namespace App\DataFixtures;

use App\Entity\HeroSlider;
use App\Entity\Sport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class HeroSliderFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_HERO_SLIDER = 3;
    /**
     * @var UploaderHelper
     */
    private $uploaderHelper;

    private $heroImages = [
        'agassi.jpeg',
        'leverdez.jpg',
        'navarro.jpg',
    ];

    public function __construct(UploaderHelper $uploaderHelper)
    {
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $sportRepository = $manager->getRepository(Sport::class);
        $sports = $sportRepository->findAll();

        for ($i = 0; $i < self::NB_HERO_SLIDER; ++$i) {
            $heroSlider = new HeroSlider();
            $heroSlider->setTitle($faker->sentence(3));
            $heroSlider->setContent($faker->text);
            $heroSlider->setSport($faker->randomElement($sports));
            $heroSlider->setImageFile(new File(__DIR__.'/images/hero/'.$this->heroImages[$i]));
            $heroSlider->setImage($this->heroImages[$i]);
            copy(__DIR__.'/images/hero/'.$this->heroImages[$i], 'public/uploads/slider/'.$this->heroImages[$i]);

            $manager->persist($heroSlider);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SportFixtures::class,
        ];
    }
}
