<?php

namespace App\DataFixtures;

use App\Entity\Matchmaking;
use App\Entity\User;
use App\Entity\UserMatchmaking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class UserMatchmakingFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $userMatchmakingRepository = $manager->getRepository(UserMatchmaking::class);
        $matchmakingRepository = $manager->getRepository(Matchmaking::class);
        $matchmakings = $matchmakingRepository->findAll();

        $userRepository = $manager->getRepository(User::class);
        $allUsers = $userRepository->findAll();
        $users = [];

        foreach ($allUsers as $key => $user) {
            foreach ($user->getRoles() as $role) {
                if ('ROLE_USER' == $role) {
                    $users[] = $user;
                }
            }
        }

        $faker = Factory::create('fr_FR');

        foreach ($matchmakings as $matchmaking) {
            $userMatchmaking = new UserMatchmaking();
            $userMatchmaking->setUser($faker->randomElement($users));
            $userMatchmaking->setMatchmaking($matchmaking);
            $userMatchmaking->setIsOrganizer(1);
            $userMatchmaking->setCancel($faker->boolean(20));
            $manager->persist($userMatchmaking);

            $manager->flush();

            if ($faker->boolean(50)) {
                $organizer = $userMatchmakingRepository->findOneBy(['matchmaking' => $matchmaking->getId()]);
                $usersWithoutOrganizer = [];
                foreach ($users as $user) {
                    if ($user != $organizer->getUser()) {
                        $usersWithoutOrganizer[] = $user;
                    }
                }
                $userMatchmaking = new UserMatchmaking();
                $userMatchmaking->setUser($faker->randomElement($usersWithoutOrganizer));
                $userMatchmaking->setMatchmaking($matchmaking);
                $userMatchmaking->setIsOrganizer(0);
                $userMatchmaking->setCancel($faker->boolean(30));
                $manager->persist($userMatchmaking);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            MatchmakingFixtures::class,
        ];
    }
}
