<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class ArticleFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_ARTICLES = 20;

    private $articleImages = [
        'article1.jpg',
        'article2.jfif',
        'article3.jfif',
        'article4.jpg',
        'article5.jpg',
        'article6.jpg',
        'article7.jpg',
        'article8.jpg',
        'article9.jpg',
        'article10.jpg',
    ];

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $tagRepository = $manager->getRepository(Tag::class);
        $tags = $tagRepository->findAll();
        $nbTags = (new TagFixtures())->nbTags;

        for ($i = 0; $i < self::NB_ARTICLES; ++$i) {
            $article = new Article();
            $article->setTitle($faker->sentence(3));
            $article->setContent($faker->text);
            $article->setExcerpt($faker->text);
            $article->addTag($tags[rand(0, $nbTags - 1)]);
            $article->setPublishedAt($faker->dateTimeBetween('-30 days', 'now'));
            $article->setIsHighlight($faker->boolean(10));

            $imageName = $faker->randomElement($this->articleImages);
            $article->setImageFile(new File(__DIR__.'/images/article/'.$imageName));
            $article->setImage($imageName);
            copy(__DIR__.'/images/article/'.$imageName, 'public/uploads/article/'.$imageName);

            $manager->persist($article);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            TagFixtures::class,
        ];
    }
}
