<?php

namespace App\DataFixtures;

use App\Entity\HighlightPartnerType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class HighlightPartnerTypeFixtures extends Fixture
{
    protected $highlightPartnerTypes;

    public $nbHighlightPartnerTypes;

    public function __construct()
    {
        $this->highlightPartnerTypes = [
            'Partenaire du mois',
            'Nouveau partenaire',
            'Partenaire de l\'année',
        ];

        $this->nbHighlightPartnerTypes = count($this->highlightPartnerTypes);
    }

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->highlightPartnerTypes as $highlightPartnerTypeName) {
            $highlightPartnerType = new HighlightPartnerType();
            $highlightPartnerType->setLabel($highlightPartnerTypeName);
            $manager->persist($highlightPartnerType);
        }

        $manager->flush();
    }
}
