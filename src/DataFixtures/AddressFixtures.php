<?php

namespace App\DataFixtures;

use App\Entity\Address;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class AddressFixtures extends Fixture
{
    const NB_ADDRESS = 50;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_ADDRESS; ++$i) {
            $address = new Address();
            $address->setStreet($faker->streetAddress);
            $address->setPostalCode($faker->postcode);
            $address->setCountry('France');
            $address->setRegion($faker->region);
            $address->setCity($faker->city);
            $address->setLatitude($faker->latitude(41.36, 51.09));
            $address->setLongitude($faker->longitude(-5.14, 9.56));

            $manager->persist($address);
        }

        $manager->flush();
    }
}
