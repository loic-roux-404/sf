<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Sport;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_ADMIN = 1;
    const NB_USER = 10;
    const NB_PARTNER = 10;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $sportRepository = $manager->getRepository(Sport::class);
        $sports = $sportRepository->findAll();
        $addressRepository = $manager->getRepository(Address::class);

        $faker = Factory::create('fr_FR');

        $user = new User();
        $user->setEmail('admin@example.com');
        $user->setUsername('admin');
        $user->setPlainPassword('password');
        $user->setEnabled(true);
        $user->addRole('ROLE_SUPER_ADMIN');
        $manager->persist($user);

        for ($i = 0; $i < self::NB_USER; ++$i) {
            $user = new User();
            if (0 == $i) {
                $user->setEmail('johndoe@example.com');
                $user->setUsername('JohnDoe');
                $user->setUsernameCanonical($user->getUsername());
            } else {
                $user->setEmail($faker->safeEmail);
                $user->setUsername($faker->userName);
                $user->setUsernameCanonical($user->getUsername());
            }
            $user->setAddress($addressRepository->findOneBy(['id' => rand(1, AddressFixtures::NB_ADDRESS)]));
            $user->setPlainPassword('password');
            $user->setEnabled(true);
            $user->addSport($sports[rand(0, SportFixtures::NB_SPORT - 1)]);
            $user->addRole('ROLE_USER');
            $user->setNotification($faker->boolean(50));
            $user->setNewsletter($faker->boolean(50));
            $manager->persist($user);
        }

        for ($i = 0; $i < self::NB_PARTNER; ++$i) {
            $user = new User();
            if (0 == $i) {
                $user->setEmail('partner@example.com');
                $user->setUsername('Partner');
                $user->setUsernameCanonical($user->getUsername());
                $user->setPartnerValidated(true);
            } else {
                $user->setEmail($faker->safeEmail);
                $user->setUsername($faker->userName);
                $user->setUsernameCanonical($user->getUsername());
                $user->setPartnerValidated($faker->boolean(80));
            }
            $user->setAddress($addressRepository->findOneBy(['id' => rand(1, AddressFixtures::NB_ADDRESS)]));
            $user->setPlainPassword('password');
            $user->setEnabled(true);
            $user->setPartner(true);
            $user->setFirstName($faker->firstName);
            $user->setLastName($faker->lastName);
            $user->setSocialReason($faker->companySuffix);
            $user->setCommercialName($faker->company);
            $user->addRole('ROLE_PARTNER');
            $user->setNotification($faker->boolean(50));
            $user->setNewsletter($faker->boolean(50));
            $manager->persist($user);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            AddressFixtures::class,
            SportFixtures::class,
        ];
    }
}
