<?php

namespace App\DataFixtures;

use App\Entity\EventType;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class EventTypeFixtures extends Fixture
{
    protected $eventTypes;

    public $nbEventTypes;

    public function __construct()
    {
        $this->eventTypes = [
            [
                'label' => 'Tournoi',
                'slug' => 'tournoi',
            ],
            [
                'label' => 'Cours en groupe',
                'slug' => 'cours-en-groupe',
            ],
            [
                'label' => 'Stage intensif',
                'slug' => 'stage-intensif',
            ],
            [
                'label' => 'Cours particulier',
                'slug' => 'cours-particulier',
            ],
            [
                'label' => 'Séance de dédicace',
                'slug' => 'seance-de-dedicace',
            ],
        ];

        $this->nbEventTypes = count($this->eventTypes);
    }

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->eventTypes as $eventTypeItem) {
            $tag = new EventType();
            $tag->setLabel($eventTypeItem['label']);
            $tag->setSlug($eventTypeItem['slug']);
            $manager->persist($tag);
        }

        $manager->flush();
    }
}
