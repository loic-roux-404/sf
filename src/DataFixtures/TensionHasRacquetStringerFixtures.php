<?php

namespace App\DataFixtures;

use App\Entity\RacquetStringer;
use App\Entity\Sport;
use App\Entity\TensionHasRacquetStringer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class TensionHasRacquetStringerFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $sportRepository = $manager->getRepository(Sport::class);

        for ($i = 0; $i < 10; ++$i) {
            $tensionHasRacquetStringer = new TensionHasRacquetStringer();
            $tensionHasRacquetStringer->setRacquetStringer($manager->getRepository(RacquetStringer::class)->find(rand(1, RacquetStringerFixtures::NB_RACQUET_STRINGER)));
            $tensionHasRacquetStringer->setSport($sportRepository->findOneBy(['id' => rand(1, SportFixtures::NB_SPORT)]));
            $tensionHasRacquetStringer->setMinTension($faker->numberBetween(15, 20).'kg');
            $tensionHasRacquetStringer->setMaxTension($faker->numberBetween(25, 30).'kg');
            $manager->persist($tensionHasRacquetStringer);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RacquetStringerFixtures::class,
            SportFixtures::class,
        ];
    }
}
