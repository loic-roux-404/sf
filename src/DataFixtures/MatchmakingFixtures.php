<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Court;
use App\Entity\Level;
use App\Entity\Matchmaking;
use App\Entity\Sport;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class MatchmakingFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_MATCHMAKINGS = 26;

    public function load(ObjectManager $manager)
    {
        $levelRepository = $manager->getRepository(Level::class);
        $levels = $levelRepository->findAll();

        $courtRepository = $manager->getRepository(Court::class);
        $courts = $courtRepository->findAll();

        $sportRepository = $manager->getRepository(Sport::class);
        $sports = $sportRepository->findAll();

        $addressRepository = $manager->getRepository(Address::class);
        $address = $addressRepository->findAll();

        $userRepository = $manager->getRepository(User::class);
        $allUsers = $userRepository->findAll();
        $users = [];

        foreach ($allUsers as $key => $user) {
            foreach ($user->getRoles() as $role) {
                if ('ROLE_USER' == $role) {
                    $users[] = $user;
                }
            }
        }

        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_MATCHMAKINGS; ++$i) {
            $matchmaking = new Matchmaking();
            $matchmaking->setTitle($faker->title);
            $matchmaking->setLevel($faker->randomElement($levels));
            $matchmaking->setCourt($faker->randomElement($courts));
            $matchmaking->setSport($faker->randomElement($sports));
            $matchmaking->setExternalAddress($address[rand(0, count($address) - 1)]);

            $startDate = $faker->dateTimeBetween('-10 days', '+50 days');
            $endDate = $faker->dateTimeInInterval($startDate, '+180 minutes');
            $matchmaking->setStart($startDate);
            $matchmaking->setEnd($endDate);

            $matchmaking->setMaxPlayer($faker->numberBetween(2, 4));
            $matchmaking->setDescription($faker->text(150));
            $matchmaking->setPrice($faker->numberBetween(5, 12));
            $manager->persist($matchmaking);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            LevelFixtures::class,
            CourtFixtures::class,
            SportFixtures::class,
            AddressFixtures::class,
        ];
    }
}
