<?php

namespace App\DataFixtures;

use App\Entity\Matchmaking;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class MessageFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_MESSAGES = 20;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $matchmakingRepository = $manager->getRepository(Matchmaking::class);
        $matchmakings = $matchmakingRepository->findAll();

        $userRepository = $manager->getRepository(User::class);
        $allUsers = $userRepository->findAll();
        $users = [];

        foreach ($allUsers as $key => $user) {
            foreach ($user->getRoles() as $role) {
                if ('ROLE_USER' == $role) {
                    $users[] = $user;
                }
            }
        }

        for ($i = 0; $i < self::NB_MESSAGES; ++$i) {
            $message = new Message();

            $matchmaking = $matchmakings[rand(0, count($matchmakings) - 1)];
            $message->setMatchmaking($matchmaking);
            $message->setUser($users[rand(0, count($users) - 1)]);
            $message->setContent($faker->text);
            $message->setDate($faker->dateTimeInInterval($matchmaking->getStart(), '-3 days'));

            $manager->persist($message);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            MatchmakingFixtures::class,
            UserFixtures::class,
        ];
    }
}
