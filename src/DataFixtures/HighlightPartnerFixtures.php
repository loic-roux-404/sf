<?php

namespace App\DataFixtures;

use App\Entity\HighlightPartner;
use App\Entity\HighlightPartnerType;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class HighlightPartnerFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_HIGHLIGHT_PARTNERS = 3;

    public function load(ObjectManager $manager)
    {
        $highlightPartnerTypeRepository = $manager->getRepository(HighlightPartnerType::class);
        $highlightPartnerTypes = $highlightPartnerTypeRepository->findAll();

        $userRepository = $manager->getRepository(User::class);
        $partners = $userRepository->findByRole('ROLE_PARTNER');

        for ($i = 0; $i < self::NB_HIGHLIGHT_PARTNERS; ++$i) {
            $event = new HighlightPartner();
            $event->setHighlightType($highlightPartnerTypes[rand(0, count($highlightPartnerTypes) - 1)]);
            $event->setPartner($partners[count($partners) - 1 - $i]);
            $event->setDisplayOrder($i);

            $manager->persist($event);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            HighlightPartnerTypeFixtures::class,
        ];
    }
}
