<?php

namespace App\DataFixtures;

use App\Entity\RacquetStringer;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class RacquetStringerFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_RACQUET_STRINGER = 5;

    private $stringerImages = [
        'stringer1.jpg',
        'stringer2.jpg',
        'stringer3.jpg',
        'stringer4.jpg',
    ];

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $userRepository = $manager->getRepository(User::class);
        $partners = $userRepository->findByRole('ROLE_PARTNER');

        $faker = Faker\Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_RACQUET_STRINGER; ++$i) {
            $racquetStringer = new RacquetStringer();
            $racquetStringer->setName($faker->name);
            $racquetStringer->setDescription($faker->text(30));
            $racquetStringer->setUser($partners[count($partners) - 1 - $i]);
            $racquetStringer->setRestitutionPeriod($faker->randomFloat(1, 1, 120));
            $racquetStringer->setPaymentCash($faker->boolean(50));
            $racquetStringer->setPaymentCard($faker->boolean(50));
            $racquetStringer->setPaymentCheck($faker->boolean(50));
            $racquetStringer->setUpdatedAt($faker->dateTime('now'));

            $imageName = $faker->randomElement($this->stringerImages);
            $racquetStringer->setImageFile(new File(__DIR__.'/images/stringer/'.$imageName));
            $racquetStringer->setImage($imageName);
            copy(__DIR__.'/images/stringer/'.$imageName, 'public/uploads/stringer/'.$imageName);

            $manager->persist($racquetStringer);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
        ];
    }
}
