<?php

namespace App\DataFixtures;

use App\Entity\Shop;
use App\Entity\TimeSlotShop;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class TimeSlotShopFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_TIME_SLOT_SHOP_BY_SHOP = 5;

    const START_TIMES = [
        '08:00:00',
        '08:30:00',
        '09:00:00',
        '09:30:00',
        '10:00:00',
    ];

    const END_TIMES = [
        '17:00:00',
        '17:30:00',
        '18:00:00',
        '18:30:00',
        '19:00:00',
    ];

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $shopRepository = $manager->getRepository(Shop::class);
        $shops = $shopRepository->findAll();

        foreach ($shops as $shop) {
            for ($i = 1; $i < self::NB_TIME_SLOT_SHOP_BY_SHOP; ++$i) {
                $timeSlot = new TimeSlotShop();
                $timeSlot->setStartTime(DateTime::createFromFormat('H:i:s', self::START_TIMES[rand(0, count(self::START_TIMES) - 1)]));
                $timeSlot->setEndTime(DateTime::createFromFormat('H:i:s', self::END_TIMES[rand(0, count(self::END_TIMES) - 1)]));
                $timeSlot->setDay($i);
                $timeSlot->setShop($shop);
                $manager->persist($timeSlot);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            ShopFixtures::class,
        ];
    }
}
