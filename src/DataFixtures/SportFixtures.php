<?php

namespace App\DataFixtures;

use App\Entity\Sport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SportFixtures extends Fixture
{
    const NB_SPORT = 4;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $sport = new Sport();
        $sport->setName('Tennis');
        $manager->persist($sport);

        $sport = new Sport();
        $sport->setName('Badminton');
        $manager->persist($sport);

        $sport = new Sport();
        $sport->setName('Squash');
        $manager->persist($sport);

        $sport = new Sport();
        $sport->setName('Padel');
        $manager->persist($sport);

        $manager->flush();
    }
}
