<?php

namespace App\DataFixtures;

use App\Entity\RopeReference;
use App\Entity\Sport;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class RopeReferenceFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_ROPE_REFERENCE = 20;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $sportRepository = $manager->getRepository(Sport::class);
        $sports = $sportRepository->findAll();

        for ($i = 0; $i < self::NB_ROPE_REFERENCE; ++$i) {
            $ropeReference = new RopeReference();
            $ropeReference->setSport($sports[rand(0, count($sports) - 1)]);
            $ropeReference->setLabel('Cordes '.$faker->isbn13);
            $manager->persist($ropeReference);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            SportFixtures::class,
        ];
    }
}
