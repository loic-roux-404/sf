<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\Level;
use App\Entity\Sport;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class EventFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_EVENTS = 50;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $levelRepository = $manager->getRepository(Level::class);
        $levels = $levelRepository->findAll();

        $sportRepository = $manager->getRepository(Sport::class);
        $sports = $sportRepository->findAll();

        $addressRepository = $manager->getRepository(Address::class);
        $address = $addressRepository->findAll();

        $eventTypeRepository = $manager->getRepository(EventType::class);
        $eventTypes = $eventTypeRepository->findAll();

        $userRepository = $manager->getRepository(User::class);
        $partners = $userRepository->findByRole('ROLE_PARTNER');

        for ($i = 0; $i < self::NB_EVENTS; ++$i) {
            $event = new Event();
            $event->setName($faker->name);
            $event->setLevel($levels[rand(0, count($levels) - 1)]);
            $event->setSport($sports[rand(0, count($sports) - 1)]);
            $event->setAddress($address[rand(0, count($address) - 1)]);
            $event->setEventType($eventTypes[rand(0, count($eventTypes) - 1)]);
            $event->setUser($partners[rand(0, count($partners) - 1)]);
            $event->setPrice($faker->randomFloat(2, 0, 200));

            $startDate = $faker->dateTimeBetween('-10 days', '+50 days');
            $endDate = $faker->dateTimeInInterval($startDate, '+3 days');
            $event->setStart($startDate);
            $event->setEnd($endDate);

            $event->setDescription($faker->text(150));
            $event->setChild($faker->boolean(20));
            $event->setUrlLink($faker->url);
            $event->setNbMaxPeople($faker->numberBetween(2, 500));

            $manager->persist($event);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            LevelFixtures::class,
            SportFixtures::class,
            AddressFixtures::class,
            EventTypeFixtures::class,
            UserFixtures::class,
        ];
    }
}
