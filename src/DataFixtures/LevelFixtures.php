<?php

namespace App\DataFixtures;

use App\Entity\Level;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LevelFixtures extends Fixture
{
    const NB_LEVEL = 3;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $level = new Level();
        $level->setLabel('Débutant');
        $level->setSlug('debutant');
        $level->setSportLevelUserSportId(rand(1, SportFixtures::NB_SPORT));
        $level->setSportLevelUserUserId(rand(1, UserFixtures::NB_USER));
        $manager->persist($level);

        $level = new Level();
        $level->setLabel('Intermédiaire');
        $level->setSlug('intermediaire');
        $level->setSportLevelUserSportId(rand(1, SportFixtures::NB_SPORT));
        $level->setSportLevelUserUserId(rand(1, UserFixtures::NB_USER));
        $manager->persist($level);

        $level = new Level();
        $level->setLabel('Avancé');
        $level->setSlug('avance');
        $level->setSportLevelUserSportId(rand(1, SportFixtures::NB_SPORT));
        $level->setSportLevelUserUserId(rand(1, UserFixtures::NB_USER));
        $manager->persist($level);

        $manager->flush();
    }
}
