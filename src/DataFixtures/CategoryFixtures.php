<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    protected $categories;

    public $nbCategories;

    public function __construct()
    {
        $this->categories = [
            'FAQ',
            'Horaires',
            'Accessibilité',
            'Qui sommes nous ?',
            'Progresser',
        ];

        $this->nbCategories = count($this->categories);
    }

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->categories as $categoryName) {
            $category = new Category();
            $category->setName($categoryName);
            $manager->persist($category);
        }

        $manager->flush();
    }
}
