<?php

namespace App\DataFixtures;

use App\Entity\RacquetStringer;
use App\Entity\RacquetStringerHasRopeReference;
use App\Entity\RopeReference;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class RacquetStringerHasRopeReferenceFixtures extends Fixture implements DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $racquetStringerRepository = $manager->getRepository(RacquetStringer::class);
        $racquetStringers = $racquetStringerRepository->findAll();

        $ropeReferencesRepository = $manager->getRepository(RopeReference::class);
        $ropeReferences = $ropeReferencesRepository->findAll();

        foreach ($racquetStringers as $racquetStringer) {
            $aleatoryRopeReferenceKey = rand(0, count($ropeReferences) - 1 - 3);
            for ($i = 0; $i < 3; ++$i) {
                $racquetStringersHasRopeReference = new RacquetStringerHasRopeReference();
                $racquetStringersHasRopeReference->setRacquetStringer($racquetStringer);
                $racquetStringersHasRopeReference->setRopeReferences($ropeReferences[$aleatoryRopeReferenceKey + $i]);
                $racquetStringersHasRopeReference->setPrice($faker->randomFloat(0, 3, 30));
                $manager->persist($racquetStringersHasRopeReference);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RacquetStringerFixtures::class,
            RopeReferenceFixtures::class,
        ];
    }
}
