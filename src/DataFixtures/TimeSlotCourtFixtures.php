<?php

namespace App\DataFixtures;

use App\Entity\Court;
use App\Entity\TimeSlotCourt;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class TimeSlotCourtFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_TIME_SLOT_BY_COURT = 5;

    const START_TIMES = [
        '08:00:00',
        '09:00:00',
        '10:00:00',
        '11:00:00',
        '12:00:00',
        '13:00:00',
        '14:00:00',
        '15:00:00',
        '16:00:00',
        '17:00:00',
        '18:00:00',
        '19:00:00',
        '20:00:00',
    ];

    const END_TIMES = [
        '09:00:00',
        '10:00:00',
        '11:00:00',
        '12:00:00',
        '13:00:00',
        '14:00:00',
        '15:00:00',
        '16:00:00',
        '17:00:00',
        '18:00:00',
        '19:00:00',
        '20:00:00',
        '21:00:00',
    ];

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $courtRepository = $manager->getRepository(Court::class);
        $courts = $courtRepository->findAll();

        foreach ($courts as $court) {
            for ($i = 1; $i < self::NB_TIME_SLOT_BY_COURT; ++$i) {
                for ($j = 0; $j < count(self::START_TIMES); ++$j) {
                    $timeSlot = new TimeSlotCourt();
                    $timeSlot->setStartTime(DateTime::createFromFormat('H:i:s', self::START_TIMES[$j]));
                    $timeSlot->setEndTime(DateTime::createFromFormat('H:i:s', self::END_TIMES[$j]));
                    $timeSlot->setDay($i);
                    $timeSlot->setCourt($court);
                    $timeSlot->setIsFullPrice($faker->boolean(30));
                    $manager->persist($timeSlot);
                }
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CourtFixtures::class,
        ];
    }
}
