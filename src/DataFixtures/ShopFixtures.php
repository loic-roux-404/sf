<?php

namespace App\DataFixtures;

use App\Entity\Brand;
use App\Entity\Shop;
use App\Entity\Sport;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\HttpFoundation\File\File;

class ShopFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_SHOP = 10;

    private $shopImages = [
        'shop1.jpg',
        'shop2.jpg',
        'shop3.jpg',
        'shop4.jpg',
        'shop5.jpg',
        'shop6.jpg',
    ];

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $brandRepository = $manager->getRepository(Brand::class);
        $brands = $brandRepository->findAll();

        $sportRepository = $manager->getRepository(Sport::class);
        $sports = $sportRepository->findAll();

        $userRepository = $manager->getRepository(User::class);
        $partners = $userRepository->findByRole('ROLE_PARTNER');

        // créations des shops
        for ($i = 0; $i < self::NB_SHOP; ++$i) {
            $shop = new Shop();
            $shop->setName($faker->company);
            $shop->setPhone($faker->phoneNumber);
            $shop->setEmail($faker->safeEmail);
            $shop->setLink($faker->url);
            $shop->setDescription($faker->text);
            $shop->setUpdatedAt($faker->dateTime());
            $shop->addSport($sports[rand(0, count($sports) - 1)]);
            $shop->setUser($partners[rand(0, count($partners) - 1)]);

            $aleatoryBrandKey = rand(0, count($brands) - 4);
            $shop->addBrand($brands[$aleatoryBrandKey]);
            $shop->addBrand($brands[$aleatoryBrandKey + 1]);
            $shop->addBrand($brands[$aleatoryBrandKey + 2]);

            $imageName = $faker->randomElement($this->shopImages);
            $shop->setImageFile(new File(__DIR__.'/images/shop/'.$imageName));
            $shop->setImage($imageName);
            copy(__DIR__.'/images/shop/'.$imageName, 'public/uploads/shop/'.$imageName);

            $manager->persist($shop);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            BrandFixtures::class,
            SportFixtures::class,
        ];
    }
}
