<?php

namespace App\DataFixtures;

use App\Entity\RacquetStringer;
use App\Entity\TimeSlotStringer;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class TimeSlotStringerFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_TIME_SLOT_SHOP_BY_RACQUET_STRINGER = 5;

    const START_TIMES = [
        '08:00:00',
        '08:30:00',
        '09:00:00',
        '09:30:00',
        '10:00:00',
    ];

    const END_TIMES = [
        '17:00:00',
        '17:30:00',
        '18:00:00',
        '18:30:00',
        '19:00:00',
    ];

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        $racquetStringerRepository = $manager->getRepository(RacquetStringer::class);
        $racquetStringers = $racquetStringerRepository->findAll();

        foreach ($racquetStringers as $racquetStringer) {
            for ($i = 1; $i < self::NB_TIME_SLOT_SHOP_BY_RACQUET_STRINGER; ++$i) {
                $timeSlot = new TimeSlotStringer();
                $timeSlot->setStartTime(DateTime::createFromFormat('H:i:s', self::START_TIMES[rand(0, count(self::START_TIMES) - 1)]));
                $timeSlot->setEndTime(DateTime::createFromFormat('H:i:s', self::END_TIMES[rand(0, count(self::END_TIMES) - 1)]));
                $timeSlot->setDay($i);
                $timeSlot->setRacquetStringer($racquetStringer);
                $manager->persist($timeSlot);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            RacquetStringerFixtures::class,
        ];
    }
}
