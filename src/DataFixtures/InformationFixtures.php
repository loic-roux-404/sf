<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Information;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class InformationFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_INFORMATION = 20;

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $categoryRepository = $manager->getRepository(Category::class);
        $categories = $categoryRepository->findAll();

        for ($i = 0; $i < self::NB_INFORMATION; ++$i) {
            $information = new Information();
            $information->setTitle($faker->sentence(3));
            $information->setContent($faker->text);
            $information->addCategory($categories[rand(0, count($categories) - 1)]);

            $manager->persist($information);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class,
        ];
    }
}
