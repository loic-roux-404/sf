<?php

namespace App\DataFixtures;

use App\Entity\Address;
use App\Entity\Court;
use App\Entity\Sport;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\HttpFoundation\File\File;

class CourtFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_COURT = 10;

    private $courtImages = [
        'court1.jpg',
        'court2.jpg',
        'court3.jpg',
        'court4.jpg',
        'court5.jpg',
        'court6.jpg',
    ];

    public function load(ObjectManager $manager)
    {
        $sportRepository = $manager->getRepository(Sport::class);
        $addressRepository = $manager->getRepository(Address::class);
        $userRepository = $manager->getRepository(User::class);

        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < self::NB_COURT; ++$i) {
            $court = new Court();

            $court->setName($faker->name);
            $court->setSport($sportRepository->findOneBy(['id' => rand(1, SportFixtures::NB_SPORT)]));
            $court->setAddress($addressRepository->findOneBy(['id' => rand(1, AddressFixtures::NB_ADDRESS)]));
            $court->setUser($userRepository->findOneBy(['id' => rand((UserFixtures::NB_ADMIN + UserFixtures::NB_USER), (UserFixtures::NB_ADMIN + UserFixtures::NB_USER + UserFixtures::NB_PARTNER))]));
            $court->setDescription($faker->text(150));
            $court->setNbCourt($faker->numberBetween(1, 8));
            $court->setIsIndoor($faker->boolean(70));
            $court->setFullPrice($faker->randomFloat(0, 30, 100));
            $court->setLowPrice($faker->randomFloat(0, 3, 16));
            $court->setUrlLink($faker->url);

            $imageName = $faker->randomElement($this->courtImages);
            $court->setImageFile(new File(__DIR__.'/images/court/'.$imageName));
            $court->setImage($imageName);
            copy(__DIR__.'/images/court/'.$imageName, 'public/uploads/court/'.$imageName);

            $manager->persist($court);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            AddressFixtures::class,
            SportFixtures::class,
        ];
    }
}
