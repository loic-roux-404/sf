<?php

namespace App\DataFixtures;

use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class TagFixtures extends Fixture
{
    protected $tags;

    public $nbTags;

    public function __construct()
    {
        $this->tags = [
            'Tennis',
            'Badminton',
            'Squash',
            'Padel',
            'Haut niveau',
            'Article de fond',
            'Enquête',
        ];

        $this->nbTags = count($this->tags);
    }

    /**
     * Load data fixtures with the passed EntityManager.
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->tags as $tagName) {
            $tag = new Tag();
            $tag->setName($tagName);
            $manager->persist($tag);
        }

        $manager->flush();
    }
}
