<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // TODO : de Cam à Arnaud : est-ce qu'on ne doit pas indiquer ici, pour les champs auxquels ca s'applique, " 'required' => true" ?
            // Step 1
            ->add('lastName')
            ->add('firstName')
            ->add('email')

            // Step 2
            ->add('username')
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les deux mots de passe doivent être identiques',
                'required' => true,
                'first_options' => ['label' => 'Mot de passe'],
                'second_options' => ['label' => 'Confirmer le mot de passe'],
                //                'options' => ['attr' => ['class' => 'password-field']],
            ])

            // Step 3
            ->add('socialReason')
            ->add('commercialName')
            ->add('partnerSiren')

            // Step 4
            ->add('address', AddressType::class, ['label_attr' => ['hidden' => true]])

            // Step 5
            ->add('partnerReason')
//            ->add('rgpd', CheckboxType::class, [
//                'required' => true,
//            ])

            // Hidden fields
            ->add('roles', null, [
                'label_attr' => [
                    'hidden' => true,
                ],
                'attr' => [
                    'hidden' => true,
                ],
            ])
            ->add('partner', null, [
                'label_attr' => [
                    'hidden' => true,
                ],
                'attr' => [
                    'checked' => true,
                    'hidden' => true,
                ],
            ])

            // Submit button
            ->add('save', SubmitType::class, ['label' => 'Valider mon inscription',
                'attr' => [
                    'hidden' => true,
                ],
            ])

//            TODO : De Cam à Arnaud : J'ai commenté tout ces champs parce que je n'en ai pas besoin pour l'inscription,
//            TODO : Mais je ne sais pas si ca pose problème pour le back de les retirer complètement, je te laisse vérifier merci :)
//            ->add('usernameCanonical')
//            ->add('emailCanonical')
//            ->add('enabled')
//            ->add('salt')
//            ->add('lastLogin')
//            ->add('confirmationToken')
//            ->add('passwordRequestedAt')
//            ->add('partnerValidated')
//            ->add('partnerValidationDate')
//            ->add('sports')
//            ->add('partnerRacquetStringer') // TODO Supprimer ce champ, car pour moi (CAM) ce champ n'a pas lieu d'être car un partenaire, à son inscription, n'a aucune presta.
//            ->add('partnerShop') // TODO Idem
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
