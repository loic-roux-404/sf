<?php

namespace App\Form;

use App\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('postalCode')
            ->add('latitude', null, ['label_attr' => ['hidden' => true], 'attr' => ['hidden' => true]])
            ->add('longitude', null, ['label_attr' => ['hidden' => true], 'attr' => ['hidden' => true]])
            ->add('street', null, ['attr' => ['class' => 'address-geoloc', 'id' => 'address-geoloc']])
            //->add('complement')
            ->add('region')
            ->add('city')
            ->add('country')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
