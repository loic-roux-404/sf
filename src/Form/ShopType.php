<?php

namespace App\Form;

use App\Entity\Brand;
use App\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('link')
            ->add('description')
            ->add('phone')
            ->add('email')
            ->add('address', AddressType::class, ['label_attr' => ['hidden' => true]])
            ->add('user', null, ['label_attr' => ['hidden' => true], 'attr' => ['hidden' => true]])
            ->add('brands', EntityType::class, [
                'class' => Brand::class,
                'choice_label' => 'label',
                'attr' => ['class' => 'tag-brands', 'name' => 'brand[]', 'multiple' => 'multiple'],
            ])
            ->add('sports', null, ['expanded' => true])
            ->add('imageFile', FileType::class)
            ->add('save', SubmitType::class, ['label' => 'Créer',
                'attr' => ['class' => 'btn-small--blue save-btn'], ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Shop::class,
        ]);
    }
}
