<?php

namespace App\Form;

use App\Entity\Level;
use App\Entity\Search\MatchmakingSearch;
use App\Entity\Sport;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MatchmakingSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sports', EntityType::class, [
                'class' => Sport::class,
                'choice_label' => 'name',
                'label' => 'Sport recherché',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('levels', EntityType::class, [
                'class' => Level::class,
                'choice_label' => 'label',
                'label' => 'Niveau de jeu',
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('max_price', IntegerType::class, [
                'label' => 'Tarif maximum',
                'required' => false,
            ])
            ->add('localisation', TextType::class, [
                'label' => 'Localisation',
                'required' => false,
            ])
            ->add('latitude', TextType::class, [
                'required' => false,
                'attr' => [
                    'hidden' => true,
                ],
            ])
            ->add('longitude', TextType::class, [
                'required' => false,
                'attr' => [
                    'hidden' => true,
                ],
            ])
            ->add('perimeter', RangeType::class, [
                'label' => 'Périmètre de recherche',
                'required' => false,
                'attr' => [
                    'min' => 10,
                    'max' => 50,
                ],
            ])
            ->add('duration', IntegerType::class, [
                'label' => 'Durée',
                'required' => false,
            ])
            ->add('datetime', TextType::class, [
                'label' => 'Date et heure',
                'required' => false,
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MatchmakingSearch::class,
            'method' => 'get',
            'csrf_protection' => false,
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}
