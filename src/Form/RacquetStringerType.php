<?php

namespace App\Form;

use App\Entity\RacquetStringer;
use App\Entity\RopeReference;
use App\Entity\Sport;
use App\Repository\RopeReferenceRepository;
use App\Repository\SportRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RacquetStringerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('sport', EntityType::class, [
                'class' => Sport::class,
                'query_builder' => function (SportRepository $em) {
                    return $em->createQueryBuilder('u')
                        ->where('u.id != 1');
                },
                'expanded' => true,
                'multiple' => false,
                'attr' => [
                    'class' => 'choice',
                ],
            ])
            ->add('price', IntegerType::class, [
                'attr' => [
                    'class' => 'max-price',
                    'min' => 0,
                ],
                'label' => 'Tarif de la pose',
            ])->add('ropeReferences', EntityType::class, [
                'class' => RopeReference::class,
                'query_builder' => function (RopeReferenceRepository $em) {
                    return $em->createQueryBuilder('r')
                    ->where('r.sport != 1');
                },
                'attr' => [
                  'class' => 'select-reference',
                ],
                'label' => 'Références',
                'choice_label' => 'label',
            ])
            ->add('ropeReferencesPrice', IntegerType::class, [
                'label' => 'Prix',
                'attr' => [
                    'class' => 'max-price',
                ],
            ])
            ->add('maxTension', RangeType::class, [
                'attr' => [
                    'class' => 'input-range max-weight-range',
                    'min' => '1',
                    'max' => '100',
                    'value' => '1',
                ],
            ])
            ->add('minTension', RangeType::class, [
                'attr' => [
                    'class' => 'input-range min-weight-range',
                    'min' => '1',
                    'max' => '100',
                    'value' => '1',
                ],
            ])
            /*->add('timeSlots', TimeSlotStringerType::class, [
                'data_class' => null,
            ])*/
            ->add('description', TextareaType::class, [
                'attr' => [
                    'placeholder' => 'Description',
                    'row' => '10',
                ],
                'label' => 'Description',
            ])
            ->add('restitutionPeriod', null, [
                'label' => 'Durée moyenne pose de cordage',
            ])
            ->add('image', FileType::class, [
                'attr' => [
                    'id' => 'file-img',
                ],
                'label' => 'Photo du magasin',
            ])
            ->add('paymentCard', ChoiceType::class, [
                'choices' => [
                    'CB' => 1,
                ],
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('paymentCash', ChoiceType::class, [
                'choices' => [
                    'Espèce' => 1,
                ],
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('paymentCheck', ChoiceType::class, [
                'choices' => [
                    'Chèque' => 1,
                ],
                'expanded' => true,
                'multiple' => true,
            ])
            ->add('day', ChoiceType::class, [
                'choices' => [
                    'Lundi' => 1,
                    'Mardi' => 2,
                    'Mercredi' => 3,
                    'Jeudi' => 4,
                    'Vendredi' => 5,
                    'Samedi' => 6,
                    'Dimanche' => 7,
                ],
            ])
            ->add('start_date', TimeType::class, [
                'input' => 'datetime',
                'html5' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time-picker',
                ],
            ])
            ->add('end_date', TimeType::class, [
                'input' => 'datetime',
                'html5' => false,
                'widget' => 'single_text',
                'attr' => [
                    'class' => 'time-picker',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => RacquetStringer::class,
        ]);
    }
}
