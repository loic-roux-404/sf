<?php

namespace App\Controller;

use App\Entity\Matchmaking;
use App\Entity\Search\MatchmakingSearch;
use App\Form\MatchmakingSearchType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MatchmakingController extends AbstractController
{
    const NB_MATCHMAKINKS_PER_PAGE = 12;

    /**
     * @Route("/avec-qui-jouer/", name="who_to_play_with")
     */
    public function index(EntityManagerInterface $manager, PaginatorInterface $paginator, Request $request)
    {
        $search = new MatchmakingSearch();
        $form = $this->createForm(MatchmakingSearchType::class, $search);
        $form->handleRequest($request);

        $repository = $this->getDoctrine()->getRepository(Matchmaking::class);
        $matchmakings = $repository->findByDateAndUserSports($this->getUser(), $search);

        $pagination = $paginator->paginate(
            $matchmakings,
            $request->query->getInt('page', 1),
            self::NB_MATCHMAKINKS_PER_PAGE
        );

        return $this->render('who-to-play-with/index.html.twig', [
            'controller_name' => 'MatchmakingController',
            'matchmakings' => $pagination,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/avec-qui-jouer/{id}/", name="who_to_play_with_show")
     */
    public function show(int $id)
    {
        $matchmakingRepository = $this->getDoctrine()->getRepository(Matchmaking::class);
        $matchmaking = $matchmakingRepository->findOneBy(['id' => $id]);

        return $this->render('who-to-play-with/show.html.twig', [
            'controller_name' => 'MatchmakingController',
            'matchmaking' => $matchmaking,
        ]);
    }
}
