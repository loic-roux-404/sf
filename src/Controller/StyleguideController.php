<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StyleguideController extends AbstractController
{
    /**
     * @Route("/styleguide/", name="styleguide")
     */
    public function index()
    {
        return $this->render('styleguide/styleguide.html.twig', [
            'controller_name' => 'StyleguideController',
        ]);
    }
}
