<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WhereToProgressController extends AbstractController
{
    /**
     * @Route("/ou-progresser/", name="where_to_progress")
     */
    public function index()
    {
        return $this->render('where-to-progress/index.html.twig', [
            'controller_name' => 'WhereToProgressController',
        ]);
    }
}
