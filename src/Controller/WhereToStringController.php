<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WhereToStringController extends AbstractController
{
    private $entityManager;

    /**
     * @Route("/where/to/string", name="where_to_string", methods={"GET"})
     */
    public function index()
    {
        return $this->render('where_to_string/index.html.twig', [
            'controller_name' => 'WhereToStringController',
        ]);
    }
}
