<?php

namespace App\Controller;

use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em, EventRepository $eventRepository)
    {
        $this->em = $em;
    }

    /**
     * @Route("/ou-m-equiper/", name="where_to_equip_me")
     */
    public function index()
    {
        return $this->render('where-to-equip-me/index.html.twig', [
            'controller_name' => 'ShopController',
        ]);
    }
}
