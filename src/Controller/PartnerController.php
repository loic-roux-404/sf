<?php

namespace App\Controller;

use App\Entity\Court;
use App\Entity\Prestation;
use App\Entity\User;
use App\Form\PartnerType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PartnerController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/espace-partenaire/tableau-de-bord/", name="partner_dashboard")
     */
    public function index()
    {
        $lastPresta = $this->em->getRepository(Prestation::class)
           ->findBy(
              ['user' => $this->getUser()->getId()],
              ['updatedAt' => 'DESC'],
              1
           )[0];

        dump($lastPresta);

        return $this->render('partner/partner-dashboard.html.twig', [
            'controller_name' => 'PartnerController',
            'user' => $this->getUser(),
            'event' => 'PartnerController',
            'active' => 'dashboard',
            'lastPresta' => $lastPresta,
            'lastPrestaEditRoute' => $lastPresta::CREATE_ROUTE_NAME,
        ]);
    }

    /**
     * @Route("/espace-partenaire/mon-profil/", name="partner_edit_profile")
     */
    public function edit()
    {
        return $this->render('partner/edit-profile.html.twig', [
            'controller_name' => 'PartnerController',
            'user' => $this->getUser(),
            'active' => 'edit-profile',
        ]);
    }

    /**
     * @Route("/espace-partenaire/mon-profil-pro/", name="partner_edit_company")
     */
    public function editCompany()
    {
        return $this->render('partner/edit-company.html.twig', [
            'controller_name' => 'PartnerController',
            'user' => $this->getUser(),
            'active' => 'edit-company',
        ]);
    }

    /**
     * @Route("/espace-partenaire/mes-services/", name="partner_services")
     */
    public function getServicesList(PaginatorInterface $paginator, Request $request): Response
    {
        $authUser = $this->getUser();
        $repository = $this->getDoctrine()->getRepository(Court::class);
        $courts = $repository->findBy(['user' => $authUser]);

        $pagination = $paginator->paginate(
            $courts,
            $request->query->getInt('page', 1),
            6);

        return $this->render('partner/services/index.html.twig', [
            'controller_name' => 'PartnerController',
            'user' => $this->getUser(),
            'pagination' => $pagination,
            'active' => 'services',
        ]);
    }

    /**
     * @Route("/espace-partenaire/preferences-de-communication/", name="partner_edit_subscriptions")
     */
    public function editSubscription()
    {
        return $this->render('partner/edit-subscriptions.html.twig', [
            'controller_name' => 'PartnerController',
            'user' => $this->getUser(),
            'active' => 'subscriptions',
        ]);
    }

    /**
     * @Route("/espace-partenaire/inscription/", name="partner_create")
     */
    public function store(Request $request, \Swift_Mailer $mailer): Response
    {
        $partenaire = new User();
//        $partenaire-> setAddress(new Address());
        $formulaire = $this->createForm(PartnerType::class, $partenaire);
        $formulaire->handleRequest($request);
        if ($formulaire->isSubmitted() && $formulaire->isValid()) {
            $this->em->persist($partenaire);
            $this->em->flush();
            $message = (new Swift_Message('Creation de compte partenaire'))
                ->setFrom('send@example.com') //TODO a congigurer
                ->setTo('recipient@example.com') //TODO a congigurer
                ->setBody(
                    $this->renderView(
                        'templates/emails/create_partner.html.twig'
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        return $this->render('partner/partner-signup.html.twig', [
            'form' => $formulaire->createView(),
        ]);
    }
}
