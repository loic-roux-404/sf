<?php

namespace App\Controller\partner;

use App\Entity\Shop;
use App\Form\ShopType;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ShopController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em, EventRepository $eventRepository)
    {
        $this->em = $em;
    }

    /**
     * @Route("/shop", name="shop")
     */
    public function index()
    {
        return $this->render('shop/index.html.twig', [
            'controller_name' => 'ShopController',
        ]);
    }

    /**
     * @Route("/espace-partenaire/nouvelle-prestation/nouveau-magasin/{id?}", name="createShop")
     */
    public function createShop(Request $request, ?Shop $shop)
    {
        $form = $this->createForm(ShopType::class, $shop ?? new Shop());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($form->getData());
            $this->em->flush();
        }

        return $this->render('partner/services/new-shop-service.html.twig', [
            'form' => $form->createView(),
            'user' => $this->getUser(),
            'active' => 'services',
        ]);
    }
}
