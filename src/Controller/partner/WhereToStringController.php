<?php

namespace App\Controller\partner;

use App\Entity\RacquetStringer;
use App\Form\RacquetStringerType;
use Doctrine\ORM\EntityManagerInterface;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class WhereToStringController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/ou-corder/", name="where_to_string")
     */
    public function index()
    {
        return $this->render('where-to-string/index.html.twig', [
            'controller_name' => 'WhereToStringController',
            'active' => 'services',
        ]);
    }

    /**
     * @Route("/espace-partenaire/prestation-de-cordage/{id}/modifier/", name="show_where_to_string")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editStringer(Request $request, RacquetStringer $racquetStringer, \Swift_Mailer $mailer)
    {
        $form = $this->createForm(RacquetStringerType::class, $racquetStringer);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $message = (new Swift_Message('Modification de prestation de cordage'))
                ->setFrom('send@example.com') //TODO a congigurer
                ->setTo('recipient@example.com') //TODO a congigurer
                ->setBody(
                    $this->renderView(
                        'templates/emails/edit_stringer.html.twig'
                    ),
                    'text/html'
                );
            $mailer->send($message);

            return $this->redirectToRoute('/espace-partenaire/prestation-de-cordage/');
        }

        return $this->render('/partner/services/edit-stringer-service.html.twig', [
            'controller_name' => 'WhereToStringController',
            'edit_stringer_presta_form' => $form,
        ]);
    }

    /**
     * @Route("/espace-partenaire/nouvelle-prestation/prestation-de-cordage/", name="create_service_stringer")
     *
     * @return string|\Symfony\Component\HttpFoundation\Response
     */
    public function createStringer(Request $request)
    {
        $form = $this->createForm(RacquetStringerType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            return 'submited';
        }

        return $this->render('/partner/services/new-stringer-service.html.twig', [
            'controller_name' => 'partner/WhereToStringController',
            'prestaform' => $form->createView(),
            'active' => 'services',
            'user' => $this->getUser(),
        ]);
    }
}
