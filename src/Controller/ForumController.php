<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ForumController extends AbstractController
{
    /**
     * @Route("/ou-en-parler/forum/", name="where_to_talk_forum")
     */
    public function index()
    {
        return $this->render('where-to-talk/forum/index.html.twig', [
            'controller_name' => 'ForumController',
        ]);
    }
}
