<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MenuController extends AbstractController
{
    /**
     * @Route("/menu/", name="menu")
     */
    public function index()
    {
        return $this->render('menu/_header_menu.html.twig', [
            'controller_name' => 'MenuController',
        ]);
    }
}
