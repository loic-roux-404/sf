<?php

namespace App\Controller;

use App\Entity\Article;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog/", name="blog")
     */
    public function index(Request $request, PaginatorInterface $paginator)
    {
        $em = $this->getDoctrine()->getManager();
        $ArticleRepository = $em->getRepository(Article::class);

        $allArticlesQuery = $ArticleRepository->findAll();

        // Paginate the results of the query
        $Articles = $paginator->paginate(
        // Doctrine Query, not results
            $allArticlesQuery,
            // Define the page parameter
            $request->query->getInt('page', 1),
            // Items per page
            1
        );

        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $Articles,
        ]);
    }

    /**
     * @Route("/blog/{slug}/", name="blog_post")
     */
    public function show(string $slug)
    {
        // ...
    }
}
