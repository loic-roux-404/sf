<?php

namespace App\Controller\user;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WhoToPlayWithController extends AbstractController
{
    /**
     * @Route("/mon-profil/matchs/", name="user_matchs")
     */
    public function index()
    {
        return $this->render('user/matchmaking.html.twig', [
            'controller_name' => 'WhoToPlayWithController',
            'user' => $this->getUser(),
            'active' => 'matchs',
        ]);
    }
}
