<?php

namespace App\Controller;

use App\Entity\Court;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CourtController extends AbstractController
{
    /**
     * @Route("/ou-jouer/", name="where_to_play")
     */
    public function index(EntityManagerInterface $manager)
    {
        $repository = $manager->getRepository(Court::class);
        $courts = $repository->findAll();

        return $this->render('where-to-play/index.html.twig', [
            'controller_name' => 'CourtController',
            'courts' => $courts,
        ]);
    }

    /**
     * @Route("/ou-jouer/{id}/", name="where_to_play_show")
     */
    public function show(int $id)
    {
        $courtRepository = $this->getDoctrine()->getRepository(Court::class);
        $court = $courtRepository->findOneBy(['id' => $id]);

        return $this->render('where-to-play/show.html.twig', [
            'controller_name' => 'CourtController',
            'court' => $court,
        ]);
    }
}
