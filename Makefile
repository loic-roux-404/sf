EXEC_PHP        = php
SYMFONY         = $(EXEC_PHP) bin/console
COMPOSER        = composer
YARN            = yarn

##
## Utils
## -----
##

database: ## Reset the database and load fixtures
database:
	$(SYMFONY) doctrine:schema:drop --full-database --force
	$(SYMFONY) doctrine:schema:update --force
	$(SYMFONY) doctrine:migrations:migrate --no-interaction --allow-no-migration
	$(SYMFONY) doctrine:fixtures:load --no-interaction

migration: ## Generate a new doctrine migration
migration:
	$(SYMFONY) doctrine:migrations:diff

entity: ## Generate a new entity
entity:
	$(SYMFONY) make:entity

cache-clear: ## Clear Symfony cache
cache-clear:
	$(SYMFONY) cache:clear

check-database: ## Validate the doctrine ORM mapping
check-database:
	$(SYMFONY) doctrine:schema:validate

compilation: ## Run Webpack Encore to compile assets
compilation:
	$(YARN) encore dev

watch: ## Run Webpack Encore in watch mode
watch:
	$(YARN) encore dev --watch

install-dependencies: ## Run Yarn and Composer to install dependencies
install-dependencies:
	$(COMPOSER) install
	$(YARN) install

code-fix: ## Run PHP CS Fixer
code-fix:
	php bin/php-cs-fixer-v2.phar fix --using-cache=no --rules=@Symfony src


##
## Quality assurance
## -----------------
##

check-twig:
	$(SYMFONY) lint:twig templates

check-yaml:
	$(SYMFONY) lint:yaml config
