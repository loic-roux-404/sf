////////////////////////////////////////////////////////////////////////////////////
//// MATCHMAKINGS SEARCH AND LIST //////////////////////////////////////////////////

$(document).ready(function () {

    $('.js-matchmakings-expandFilter').click(function () {
        let indexOfClicked = $('.js-matchmakings-expandFilter').index(this);
        $(this).find('a').toggleClass('input-label-expandButton-btnPlus');
        $(this).find('a').toggleClass('input-label-expandButton-btnMinus');
        $('.js-matchmakings-expandFilter').each(function (index, element) {
            if (index !== indexOfClicked) {
                if ($(element).find('a').hasClass('input-label-expandButton-btnMinus')) {
                    $(element).find('a').addClass('input-label-expandButton-btnPlus');
                    $(element).find('a').removeClass('input-label-expandButton-btnMinus');
                }
            }
        });
    });

    $('.js-matchmakings-toggleForm').find('span').text('Voir les filtres');
    $('.js-matchmakings-toggleForm').click(function () {
        if ($(this).find('span').text() === "Voir les filtres") {
            $(this).find('span').text('Masquer les filtres')
        } else {
            $(this).find('span').text('Voir les filtres')
        }
    });
});
