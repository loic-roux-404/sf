$(document).ready(function () {

    function getAddressesNames(features) {
        let addresses = [];
        features.forEach(function(feature) {
            addresses[feature.properties.label] = feature;
        });
        return addresses;
    }

    $('.js-autocomplete-street').keyup(function () {
        axios.get('https://api-adresse.data.gouv.fr/search/', {
            params: {
                q: $('.js-autocomplete-street').val()
            }
        })
            .then(function (response) {
                let addresses = getAddressesNames(response.data.features);
                $('.js-autocomplete-street').autocomplete({
                    source: Object.keys(addresses),
                    select: function (a, b) {
                        a.preventDefault();
                        $('.js-autocomplete-postalCode').val(addresses[b.item.label].properties.postcode);
                        $('.js-autocomplete-city').val(addresses[b.item.label].properties.city);
                        $('.js-autocomplete-lat').val(addresses[b.item.label].geometry.coordinates[1]);
                        $('.js-autocomplete-lng').val(addresses[b.item.label].geometry.coordinates[0]);
                        $('.js-autocomplete-street').val(addresses[b.item.label].properties.name);
                        $('.js-autocomplete-country').val("France");
                    },
                });

            })
    });

    $('.js-autocomplete-localisation').keyup(function () {
        if ($(this).val().length === 0) {
            $('.js-autocomplete-lat').val('');
            $('.js-autocomplete-lng').val('');
        } else {
            axios.get('https://api-adresse.data.gouv.fr/search/', {
                params: {
                    q: $('.js-autocomplete-localisation').val()
                }
            })
                .then(function (response) {
                    let addresses = getAddressesNames(response.data.features);
                    $('.js-autocomplete-localisation').autocomplete({
                        source: Object.keys(addresses),
                        select: function (a, b) {
                            $('.js-autocomplete-lat').val(addresses[b.item.label].geometry.coordinates[1]);
                            $('.js-autocomplete-lng').val(addresses[b.item.label].geometry.coordinates[0]);
                        },
                    });

                })
        }
    });

    $('.js-geolocalisation-trigger').click(function () {
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                axios.get('https://api-adresse.data.gouv.fr/reverse/', {
                    params: {
                        lat: position.coords.latitude,
                        lon: position.coords.longitude,
                    }
                })
                    .then(function (response) {
                        $('.js-geolocalisation-input').val(response.data.features[0].properties.label);
                        $('.js-autocomplete-lat').val(position.coords.latitude);
                        $('.js-autocomplete-lng').val(position.coords.longitude);
                    })
            });
        }
    })
});