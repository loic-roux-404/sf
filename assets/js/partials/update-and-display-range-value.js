$(document).ready(function () {
    $('.max-weight-range').on('input', function () {
        $('#max-weigh-ref').text(this.value + "kg")
    })
    $('.min-weight-range').on('input', function () {
        $('#min-weigh-ref').text(this.value + "kg")
    })
})
