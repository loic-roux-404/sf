// Global variables
// ===================================================================
// Starting from 320px :
$max_xxs_bkpt = 399;
$min_xs_bkpt = 400;
// Bootstrap breakpoints
// col-xs-
$max_xs_bkpt = 575
$min_sm_bkpt = 576
// col-sm-
$max_sm_bkpt = 767;
$min_md_bkpt = 768;
// col-md-
$max_md_bkpt = 991;
$min_lg_bkpt = 992;
// col-lg-
$max_lg_bkpt = 1199;
$min_xl_bkpt = 1200;
// col-xl-


var windowWitdh = 0;
var accountSidebarMenu = $('#account-sidebar-menu');
var accountSidebarMenuOverlay = $('.account-content-overlay');


// Document ready
// ===================================================================
$( document ).ready(function() {
    // Open Navigation Menu on Mobile
    $('#open-menu-mobile').click(function () {
        document.getElementById("container-header-mobile").style.width = "80%";
    });

    // Close Navigation Menu on Mobile
    $('#close-menu-mobile').click(function () {
        document.getElementById("container-header-mobile").style.width = "0%";
    });

    // Open Search container in header
    $('#search-link').click(function () {
        document.getElementById("container-search").style.height = "50px";
    });

    // Close Search container in header
    $('#btn-close-search').click(function () {
        document.getElementById("container-search").style.height = "0px";
    });

    // Add color on active link in header
    $('.link-header').click(function() {
        $('.link-header.active').removeClass("active");
        $(this).addClass("active");
    });

    // Add border bottom on active link in header
    $('#navigation-links li').click(function() {
        $('.border-bottom-link-header').css("display","none");
        $(this).children().css("display","block");
    });


    // Managing account sidebar menus
    // ===================================================================
    currentUrl = window.location.href;
    windowWitdh = window.innerWidth;
    accountSidebarMenuMobileHide();

    // Connected icon account onclick : open sidebar account menu on mobile
    $('.site-header #my-account-link').click(function(e) {
        // If mobile and "espace perso partenaire" or "espace perso utilisateur" (checked by if accountSidebarMenu exists)
        if (accountSidebarMenu.length) {
            if(windowWitdh <= $max_sm_bkpt) {
                e.preventDefault(); // Block action of link
                accountSidebarMenu.toggle( "slide" );
                accountSidebarMenuOverlay.toggle();

            }
        }
    });

    $('#account-sidebar-menu-close').click(function() {
        if(windowWitdh <= $max_sm_bkpt) {
            accountSidebarMenu.toggle( "slide" );
            accountSidebarMenuOverlay.toggle();
        }
    })


});

// Window resize
// ===================================================================
$( window ).resize(function() {
    // Managing account sidebar menus
    // ===================================================================
    windowWitdh = window.innerWidth;
    accountSidebarMenuMobileHide();
});


// Account sidebar menus : hide if mobile
// ===================================================================
function accountSidebarMenuMobileHide() {
    // If mobile and "espace perso partenaire" or "espace perso utilisateur" (checked by if accountSidebarMenu exists)
    if (accountSidebarMenu.length) {
        if(windowWitdh <= $max_sm_bkpt) {
            accountSidebarMenu.hide();
        } else {
            accountSidebarMenu.show();
        }
    }
}