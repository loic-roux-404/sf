$(document).ready(function() {
    $('.max-weight-range').on('input',function () {
        $('#max-weigh-ref').text(this.value + "kg")
    })
    $('.min-weight-range').on('input',function () {
        $('#min-weigh-ref').text(this.value + "kg")
    })
    $('#img-file-button').click(function(){
        $('#racquet_stringer_image').click()
    })
    $('#racquet_stringer_image').change(function (e) {
        let fileName = e.target.value;
        $('#selected-file').text(fileName)
    })
    $.extend(true, $.fn.datetimepicker.defaults, {
        icons: {
            time: 'fa fa-clock',
            date: 'fa-calendar',
            up: 'fa fa-arrow-up',
            down: 'fa fa-arrow-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-calendar-check',
            clear: 'fa fa-trash-alt',
            close: 'fa fa-times-circle'
        }
    });
    $(".time-picker").datetimepicker({
        format: 'H:mm',
    });
    $("#datetime").datetimepicker({
        format: 'D/MM/Y - H:mm',
    });
});