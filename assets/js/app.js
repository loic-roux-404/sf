// =============================================================================
// REQURE ALL SCSS FILES
// 
// L'ordre est important, merci de ne rien modifier sans l'accord des responsables front
// =============================================================================

// BASES
require('../sass/bases/_init.scss');
require('../sass/bases/_variables.scss');
require('../sass/bases/_mixins.scss');
require('../sass/bases/_fonts.scss');
require('../sass/bases/_tarteaucitron.scss');

// COMPONENTS
require('../sass/components/_links.scss');
require('../sass/components/_lr-titles.scss');
require('../sass/components/_buttons.scss');
require('../sass/components/_lr-pagination.scss');
require('../sass/components/_lr-avatar.scss');
require('../sass/components/_heading-img.scss');
require('../sass/components/_lr-tabs.scss');
require('../sass/components/_cards.scss');
require('../sass/components/_inputs.scss');
require('../sass/components/filters-block.scss');
require('../sass/components/_confirmation-msg.scss');
require('../sass/components/popup-delete.scss');
require('../sass/components/wheretotalk-header-tabs.scss');
require('../sass/components/form-steps.scss');
require('../sass/components/_modals.scss');

// LAYOUT ELEMENTS
require('../sass/layout-elements/header.scss');
require('../sass/layout-elements/footer.scss');
require('../sass/layout-elements/newsletter.scss');
require('../sass/layout-elements/reassurances.scss');
require('../sass/layout-elements/share-social.scss');
require('../sass/layout-elements/account-sidebar-menu.scss');

// PAGES
require('../sass/pages/_styleguide.scss');
require('../sass/pages/homepage.scss');
require('../sass/pages/page-contact.scss');
require('../sass/pages/page-infos.scss');
require('../sass/pages/reset-password.scss');
require('../sass/pages/change-password.scss');
// Where to string
require('../sass/pages/stringers-list.scss');
require('../sass/pages/stringer-details.scss');
// Where to play
require('../sass/pages/courts-list.scss');
require('../sass/pages/court-details.scss');
// Who to play with
require('../sass/pages/meetings-list.scss');
require('../sass/pages/meeting-details.scss');
require('../sass/pages/meeting-new.scss');
// Where to equip me
require('../sass/pages/shops-list.scss');
require('../sass/pages/shop-details.scss');
// Where to play
require('../sass/pages/activities-list.scss');
require('../sass/pages/activity-details.scss');
// Where to talk
require('../sass/pages/forum-list.scss');
require('../sass/pages/forum-details.scss');
require('../sass/pages/forum-new.scss');
require('../sass/pages/posts-list.scss');
require('../sass/pages/post-details.scss');
require('../sass/pages/infos-list.scss');

// Pages : user
require('../sass/pages/user/user-sidebar-register.scss');
require('../sass/pages/user/user-complete-profile.scss');
require('../sass/pages/user/user-edit-matchmaking.scss');
require('../sass/pages/user/user-edit-profile.scss');
require('../sass/pages/user/user-edit-subscriptions.scss');
require('../sass/pages/user/user-matchmaking.scss');
require('../sass/pages/user/user-public-profile.scss');
// Pages : partner
require('../sass/pages/partner/partner-dashboard.scss');
require('../sass/pages/partner/partner-edit-activity-service.scss');
require('../sass/pages/partner/partner-edit-court-service.scss');
require('../sass/pages/partner/partner-edit-profile.scss');
require('../sass/pages/partner/partner-edit-shop-service.scss');
require('../sass/pages/partner/partner-edit-stringer-service.scss');
require('../sass/pages/partner/partner-list-services.scss');
require('../sass/pages/partner/partner-new-activity-service.scss');
require('../sass/pages/partner/partner-new-court-service.scss');
require('../sass/pages/partner/partner-new-shop-service.scss');
require('../sass/pages/partner/partner-new-stringer-service.scss');
require('../sass/pages/partner/partner-signup.scss');


// =============================================================================
// REQUIRE ALL JS FILES
// =============================================================================
require('./partials/layout-elements.js');
require('./partials/header.js');
require('./partials/script.js');
require('./partials/steps-form.js');
// require('./partials/time-picker-component');
require('./partials/user-sidebar-register.js');
require('./partials/new-shop');
// require('./partials/update-and-display-range-value');
require('./partials/geolocalisation');
require('./partials/new-racket-stringer');


// =============================================================================
// REQUIRE DEPENDENCIES
// =============================================================================

const $ = require('jquery');
require('jquery-ui-bundle');
require('jquery-ui');
// require('jquery-steps/build/jquery.steps.min');
require('jquery-validation/dist/jquery.validate.min');
require('bootstrap');
require('moment');
require('pc-bootstrap4-datetimepicker');
require('pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');
require('select2/dist/js/select2.full');
window.axios = require('axios').default;

require('./partials/geolocalisation');